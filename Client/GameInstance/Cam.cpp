#include "Cam.h"


glm::vec3 Cam::absToRel(glm::vec3 update)
{
	float magnitude = 0.3f;
	glm::vec3 finalTranslation;

	if (update.z == 0.5) {
		//std::cout << "S\n";
		finalTranslation = (magnitude * -direction);
	}
	if (update.z == -0.5) {
		finalTranslation = (magnitude * direction);
	}
	if (update.x == -0.5) {
		glm::vec3 du = glm::cross(cam_up, direction);
		finalTranslation = (magnitude * du);
	}
	if (update.x == 0.5) {
		glm::vec3 du = glm::cross(cam_up, direction);
		finalTranslation = (magnitude * -du);
	}

	return finalTranslation;
}

Cam::Cam() {
	cam_pos = glm::vec3(0.0f, 0.5f, -6.75f);		// e  | Position of camera
	cam_look_at = glm::vec3(0.0f, 0.5f, 500.0f);	// d  | This is where the camera looks at	
	cam_up = glm::vec3(0.0f, 1.0f, 0.0f);
	toWorld = glm::mat4(1.0f);
	direction = cam_look_at - cam_pos;
	direction = normalize(direction);
}

Cam::~Cam() {}

void Cam::moveVV(glm::vec3 currPos) {
	
	glm::vec3 dir = - currPos + cursorPos; // current mouse position minus previous mouse position
	glm::vec3 currentPos(currPos.x, currPos.y, currPos.z);
	glm::vec3 zplane(0.0f, 0.0f, -1.0f);


	if (glm::isnan(dir.x) || glm::isnan(dir.y) || glm::isnan(dir.z))
	{
		std::cout << "DIR IS NAN\n";
		std::cout << "currPos: "<< currPos.x << currPos.y << currPos.z << std::endl;
		std::cout << "cursorPos: " << cursorPos.x << cursorPos.y << cursorPos.z << std::endl;
		return;
	}
	if (glm::isnan(currentPos.x) || glm::isnan(currentPos.y) || glm::isnan(currentPos.z))
	{
		std::cout << "currentPos IS NAN\n";
		std::cout << "currPos: "<< currPos.x << currPos.y << currPos.z << std::endl;
		std::cout << "cursorPos: " << cursorPos.x << cursorPos.y << cursorPos.z << std::endl;
		return;
	}

	float bel = length(dir); // find length of curr - prev

	if (bel > 0.0001) { // if length of curr - prev is substantial
		glm::vec4 direct(direction, 1.0f);
		float rotAngle = 0.0f;

		// find rotation angle in y axis
		rotAngle = asin(dir.y / 2.0f) * 2.0f;
		rotAngle = (rotAngle * 180.0f) / 3.141592653f; //converts radians to degrees
		glm::vec3 crossr = glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), direction);
		glLoadIdentity(); // load the identity matrix onto the matrix stack
		glRotatef(rotAngle, crossr.x, crossr.y, crossr.z); // rotate this identity matrix by angleor degrees around axisor
														   //glMultMatrixf((GLfloat *)&toWorld[0][0]); // multiply this rotated matrix by the toWorld matrix
		glGetFloatv(GL_MODELVIEW_MATRIX, (GLfloat *)&toWorld[0][0]); // overwrite the toWorld matrix with this new matrix
		direct = toWorld * direct; // translate camera look at val by rotAngle degrees about the y axis
		if (glm::isnan(direct.x) || glm::isnan(direct.y) || glm::isnan(direct.z))
		{
			std::cout << "1DIRection IS NAN\n";
			std::cout << "rotAngle: " << rotAngle << std::endl;
			std::cout << "crossr: " << glm::to_string(crossr) << std::endl;
			std::cout << "currPos: " << glm::to_string(currPos) << std::endl;
			std::cout << "cursorPos: " << glm::to_string(cursorPos) << std::endl;
			std::cout << "toWorld: " << glm::to_string(toWorld) << std::endl;
		}
		// find angle of rotation in x axis
		rotAngle = asin(dir.x / 2.0f) * 2.0f;
		rotAngle = (rotAngle * 180.0f) / 3.141592653f; //converts radians to degrees
		glLoadIdentity(); // load the identity matrix onto the matrix stack
		glRotatef(rotAngle, 0.0f, 1.0f, 0.0f); // rotate this identity matrix by angleor degrees around axisor
											   //glMultMatrixf((GLfloat *)&toWorld[0][0]); // multiply this rotated matrix by the toWorld matrix
		glGetFloatv(GL_MODELVIEW_MATRIX, (GLfloat *)&toWorld[0][0]); // overwrite the toWorld matrix with this new matrix
		direct = toWorld * direct; // translate direct (which was previously translated) by x degrees

		if (glm::isnan(direct.x) || glm::isnan(direct.y) || glm::isnan(direct.z))
			direct = glm::vec4(glm::normalize(cam_pos),1.0f);


		direction = glm::normalize(glm::vec3(direct.x, direct.y, direct.z));


		cam_look_at = cam_pos + (500.0f * direction);
	}

	cursorPos = currentPos; // set prev mouse position to new mouse position for next mouse movement
}

void Cam::translate(int ver) {
	float magnitude = 0.06f;
	glm::vec3 finalTranslation;
	if (ver == 0) {//"W"
		finalTranslation = (magnitude * direction);
	}
	else if (ver == 1) {//"A"
						//std::cout << "A\n";
		glm::vec3 du = glm::cross(cam_up, direction);
		finalTranslation = (magnitude * du);
	}
	else if (ver == 2) {//"s"
						//std::cout << "S\n";
		finalTranslation = (magnitude * -direction);
	}
	else if (ver == 3) {//"D"
						//std::cout << "D\n";
		glm::vec3 du = glm::cross(cam_up, direction);
		finalTranslation = (magnitude * -du);
	}

	printf("Cam pos: (%f,%f,%f)\n", cam_pos.x, cam_pos.y, cam_pos.z);

	cam_pos = cam_pos + finalTranslation;
	cam_look_at = cam_look_at + finalTranslation;
}

void Cam::update(glm::vec3 update)
{
	cam_pos = cam_pos + update;
	cam_look_at = cam_look_at + update;
	direction = cam_look_at - cam_pos;

}

void Cam::reset() {

	std::cout << glm::to_string(cam_pos) << std::endl;
	std::cout << glm::to_string(cam_look_at)<< std::endl;
	std::cout << glm::to_string(direction) << std::endl;
	if (cam_pos.x == 0.0f && cam_pos.y == 0.0f && cam_pos.z == 0.0f)
		cam_pos = glm::vec3(0.0f, 0.0f, 1.0f);
	// Default camera parameters
	toWorld = glm::mat4(1.0f);
	direction = glm::normalize(cam_pos);
	cam_look_at = cam_pos + direction;
}
