#pragma once
#ifndef CAM_H_
#define CAM_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include "glm/gtx/string_cast.hpp"

class Cam
{
public:
	// Default camera parameters

	Cam();
	~Cam();

	void moveVV(glm::vec3);
	void translate(int);
	void update(glm::vec3);
	void reset();

	glm::vec3 get_pos() {
		if (glm::isnan(cam_pos.x) || glm::isnan(cam_pos.y) || glm::isnan(cam_pos.z))
			std::cout << "GET POS NAN\n";
		return cam_pos;
	}
	void set_pos(glm::vec3 update) {
		if (glm::isnan(update.x) || glm::isnan(update.y) || glm::isnan(update.z)) {
			std::cout << "SET pos NAN\n";
			return;
		}
		cam_pos = update;
	}
	glm::vec3 get_look_at() {
		//if (glm::isnan(cam_look_at.x) || glm::isnan(cam_look_at.y) || glm::isnan(cam_look_at.z))
		//	std::cout << "GET LOOK AT NAN\n";
		return cam_look_at;
	}
	void set_look_at(glm::vec3 update) {
		if (glm::isnan(update.x) || glm::isnan(update.y) || glm::isnan(update.z))
			std::cout << "SET cam_look_at NAN\n";
		cam_look_at = update;
	}
	glm::vec3 get_up() {
		//if (glm::isnan(cam_up.x) || glm::isnan(cam_up.y) || glm::isnan(cam_up.z))
		//	std::cout << "GET cam_up NAN\n";
		return cam_up;
	}
	void set_up(glm::vec3 update) {
		if (glm::isnan(update.x) || glm::isnan(update.y) || glm::isnan(update.z))
			std::cout << "SET cam_up NAN\n";
		cam_up = update;
	}
	glm::vec3 get_direction() {
		if (glm::isnan(direction.x) || glm::isnan(direction.y) || glm::isnan(direction.z)) {
			std::cout << "GET direction NAN\n";
			direction = glm::normalize(cam_pos);
		}
		return direction;
	}
	void set_direction(glm::vec3 update) {
		if (glm::isnan(update.x) || glm::isnan(update.y) || glm::isnan(update.z))
			std::cout << "SET direction NAN\n";
		direction = update;
	}

private:
	glm::vec3 cam_pos;		// e  | Position of camera
	glm::vec3 cam_look_at;	// d  | This is where the camera looks at
	glm::vec3 cam_up;		// up | What orientation "up" is


	glm::mat4 toWorld;

	glm::vec3 cursorPos;
	glm::vec3 direction;
	glm::vec3 absToRel(glm::vec3);
};
#endif
