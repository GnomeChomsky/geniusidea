#include "Drawable.h"

Drawable::Drawable(Packet packet, GLuint sp) {
	objectID = packet.objID;
	position = packet.pos;
	scale = packet.scalar;
	shaderProgram = sp;

	velocity = packet.velocity;
	rotation = packet.rotation;
	if (packet.enabled == 1.0f) enabled = true;
	else enabled = false;

}

Drawable::Drawable(unsigned int objID, Model* mod, glm::vec3 pos, float scalar, GLuint sp) {
	objectID = objID;
	model = mod;
	shaderProgram = sp;
	position = pos;
	scale = scalar;

	velocity = glm::vec3(0.0f);
	rotation = glm::mat4(1.0f);
	enabled = true;
}

void Drawable::draw() {
	this->model->toWorld = glm::translate(glm::mat4(), position) * rotation * glm::scale(glm::mat4(), glm::vec3(scale)); // T * R * S
	this->model->Draw(shaderProgram);
}

void Drawable::move() {
	this->position = this->position + this->velocity;
	this->velocity.y -= 0.0007f;
	if (this->position.y <= -0.7f) this->velocity = glm::vec3(0.0f,0.0f,0.0f);
}

void Drawable::update(Packet packet) {
	position = packet.pos;
	scale = packet.scalar;

	velocity = packet.velocity;
	rotation = packet.rotation;
	if (packet.enabled == 1.0f) enabled = true;
	else enabled = false;
}

Packet Drawable::convertToPacket() {
	Packet packet;
	packet.packet_type = ACTION_EVENT;
	packet.objID = objectID;
	packet.pos = position;
	packet.scalar = scale;
	packet.rotation = rotation;
	packet.velocity = velocity;
	if (enabled) packet.enabled = 1.0f;
	else packet.enabled = 0.0f;

	return packet;
}