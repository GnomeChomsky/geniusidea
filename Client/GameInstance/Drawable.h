#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/mat4x4.hpp>

#include "Model.h"
#include "../../Network/NetworkData.h"

class Drawable
{
public:
	Drawable(Packet, GLuint);
	Drawable(unsigned int, Model*,glm::vec3, float, GLuint);

	void draw();

	Model* model;

	glm::vec3 position; // translation (offset from 0)
	glm::mat4 rotation; // local rotation
	float scale; // scalar value for dimensional proportion

	glm::vec3 velocity;

	unsigned int objectID;
	GLuint shaderProgram;
	bool enabled;

	void move();
	void update(Packet);
	Packet convertToPacket();

};