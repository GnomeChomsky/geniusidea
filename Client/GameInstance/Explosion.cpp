#include "Explosion.h"


Explosion::Explosion(ParticleGenerator* partGen, GLuint shader) {
	this->pGen = partGen;
	this->highlight = false;
	this->explosionStageTime = 8;
	this->explosionTime = 3 * this->explosionStageTime;
	this->toWorld = glm::mat4(1.0f);
	this->shaderProgram = shader;

}

void Explosion::draw() {

	glUseProgram(shaderProgram);
	if (explosionTime > 2 * explosionStageTime)
		pGen->RenderExplosion(toWorld, pGen->newParticles);
	else if (explosionTime > explosionStageTime)
		pGen->RenderExplosion(toWorld, (2 * pGen->newParticles / 3));
	else if (explosionTime > 0)
		pGen->RenderExplosion(toWorld, pGen->newParticles / 3);
	//else
		//pGen->RenderExplosion(toWorld, 0);
	explosionTime--;
}

