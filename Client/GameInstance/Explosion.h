#pragma once

#include "ParticleGenerator.h"

class Explosion
{
public:
	Explosion(ParticleGenerator*, GLuint);
	void draw();

	GLint explosionTime;
	GLint explosionStageTime;
	GLuint shaderProgram;

	ParticleGenerator * pGen;
	bool highlight;
	glm::mat4 toWorld;
	glm::vec3 center;

};