#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include "Drawable.h"

using namespace std;

class Fruit : public Drawable {
public:
	/*  Functions   */
	void split();
	void getPickedUp();
	void getThrown();
};