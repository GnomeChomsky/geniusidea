 /*******************************************************************
** This code is part of Breakout.
**
** Breakout is free software: you can redistribute it and/or modify
** it under the terms of the CC BY 4.0 license as published by
** Creative Commons, either version 4 of the License, or (at your
** option) any later version.
******************************************************************/
#include "ParticleGenerator.h"
#include "Window.h"

ParticleGenerator::ParticleGenerator(GLint shader, GLuint amount, GLfloat life, GLuint newParticles, GLfloat size, GLfloat offset, GLfloat velocity, bool engine)
	: shader(shader), amount(amount), life(life), newParticles(newParticles), size(size), offset(offset), velocity(velocity), engine(engine)
{
	this->init();
}

void ParticleGenerator::Update()
{
	GLfloat dt = (GetLife() / 50.0f);
	// Add new particles 
	for (GLuint i = 0; i < newParticles; ++i)
	{
		int unusedParticle = this->firstUnusedParticle();
		this->respawnParticle(this->particles[unusedParticle]);
	}
	// Update all particles
	for (GLuint i = 0; i < this->amount; ++i)
	{
		Particle* p = this->particles[i];
		p->Life -= dt; // reduce life
		if (p->Life > 0.0f)
		{	// particle is alive, thus update
			p->offset += p->Velocity * dt;

			// Check colored region
			if (p->Life / life < 0.8f)
				p->Color[1] = 0.5f;			
				if (p->Life / life < 0.4f)
					p->Color[1] = 0.0f;
		}
	}
}

// Render all particles
void ParticleGenerator::Draw()
{
	/*
	Update();
	// Use additive blending to give it a 'glow' effect
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glUseProgram(shader); 
	glEnable(GL_POINT_SPRITE);
	for (Particle* particle : this->particles)
	{
		if (particle->Life > 0.0f)
		{
			GLuint uOffset = glGetUniformLocation(shader, "offset");
			glUniform3fv(uOffset, 1, (const GLfloat*)glm::value_ptr(loc));
			GLint colorLoc = glGetUniformLocation(shader, "color");
			glUniform4fv(colorLoc, (GLsizei) 1, (const GLfloat*)glm::value_ptr(particle->Color));
			GLuint uProjection = glGetUniformLocation(shader, "projection");
			glUniformMatrix4fv(uProjection, 1, GL_FALSE, &(Window::P[0][0]));
			GLuint uView = glGetUniformLocation(shader, "view");
			glUniformMatrix4fv(uView, 1, GL_FALSE, &(Window::V[0][0]));

			// For billboarding
			GLuint CameraRight_ID = glGetUniformLocation(shader, "cameraRight");
			GLuint CameraUp_ID = glGetUniformLocation(shader, "cameraUp");
			glUniform3f(CameraRight_ID, Window::V[0][0], Window::V[1][0], Window::V[2][0]);
			glUniform3f(CameraUp_ID, Window::V[0][1], Window::V[1][1], Window::V[2][1]);

			// Bind texture
			glBindTexture(GL_TEXTURE_2D, texture);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texwidth, texheight, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
			glGenerateMipmap(GL_TEXTURE_2D);

			//Good pratice to free and unbind
			SOIL_free_image_data(image);
			glBindTexture(GL_TEXTURE_2D, 0);
			

			glBindVertexArray(this->VAO);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			//glDrawArrays(GL_TRIANGLES, 0, 6);
			glBindVertexArray(0);
		}
	}
	// Don't forget to reset to default blending mode
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);*/
}

struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
};

void ParticleGenerator::init()
{
	// Set up mesh and attribute properties
	GLuint VBO;

	// The VBO containing the 4 vertices of the particles.
	const GLfloat g_vertex_buffer_data[] = {
		-size, -size, 0.0f,
		size, -size, 0.0f,
		-size,  size, 0.0f,
		size,  size, 0.0f,
	};
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(this->VAO);
	// Fill mesh buffer
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), &g_vertex_buffer_data[0], GL_STATIC_DRAW);
	// Set mesh attributes
	glEnableVertexAttribArray(0);
	// Vertex Positions
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
	glBindVertexArray(0);

	glGenTextures(1, &this->texture);

	//Currently a hardcoded fire jpg
	this->image = SOIL_load_image("../models/Fire/fireTex", &texwidth, &texheight, 0, SOIL_LOAD_RGB);

	// Vertex Texture Coords
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));
	glBindVertexArray(1);


	// Create this->amount default particle instances
	for (GLuint i = 0; i < this->amount; ++i)
		this->particles.push_back(new Particle());
}

// Stores the index of the last particle used (for quick access to next dead particle)
GLuint lastUsedParticle = 0;
GLuint ParticleGenerator::firstUnusedParticle()
{
	// First search from last used particle, this will usually return almost instantly
	for (GLuint i = lastUsedParticle; i < this->amount; ++i) {
		if (this->particles[i]->Life <= 0.0f) {
			lastUsedParticle = i;
			return i;
		}
	}
	// Otherwise, do a linear search
	for (GLuint i = 0; i < lastUsedParticle; ++i) {
		if (this->particles[i]->Life <= 0.0f) {
			lastUsedParticle = i;
			return i;
		}
	}
	// All particles are taken, override the first one (note that if it repeatedly hits this case, more particles should be reserved)
	lastUsedParticle = 0;
	return 0;
}

void ParticleGenerator::respawnParticle(Particle* particle)
{
	GLfloat random1 = ((rand() % 50)) / velocity;
	GLfloat random5;
	GLfloat random6;
	if (engine) {
		random5 = ((rand() % 100) - 50) / 30.0f;
		random6 = ((rand() % 100) - 50) / 30.0f;
	}
	else {
		random5 = ((rand() % 100) - 50) / (velocity * 6);
		random6 = ((rand() % 100) - 50) / (velocity * 6);
	}

	GLfloat random2 = ((rand() % 100) - 50) / offset;
	GLfloat random3 = ((rand() % 100) - 50) / offset;
	GLfloat random4 = ((rand() % 100) - 50) / offset;

	//GLint randSign1 = (rand() % 2) - 1;
	//GLint randSign2 = (rand() % 2) - 1;


	
	//GLfloat random3 = random2 * randSign1;
	//GLfloat random4 = random2 * randSign2;


	//fprintf(stderr, "Respawaned a random at %d \n", (int) random);



	// Currently just place near the origin
	particle->offset = glm::vec3(random2, random3, random4);
	//particle.toWorld[3] = 1;
	if (engine)
		particle->Color = glm::vec4(0.0f, 1.0f, 1.0f, 0.9f);
	else
		particle->Color = glm::vec4(1.0f, 1.0f, 0.0f, 0.9f);
	particle->Life = life;
	particle->Velocity = glm::vec3(random5, random1, random6);

	//fprintf(stderr, "Respawaned a particle at %d %d %d %d \n", (int) particle.Position[0], (int) particle.Position[1], (int) particle.Position[2], (int) particle.Position[3]);
}

void ParticleGenerator::respawnParticleExplosion(Particle* particle)
{
	GLfloat random1 = ((rand() % 400) - 200) / 2.5f;
	GLfloat random5 = ((rand() % 400) - 200) / 2.5f;
	GLfloat random6 = ((rand() % 400) - 200) / 2.5f;


	GLfloat random2 = ((rand() % 100) - 50) / 100.0f;
	GLfloat random3 = ((rand() % 100) - 50) / 100.0f;
	GLfloat random4 = ((rand() % 100) - 50) / 100.0f;

	//GLint randSign1 = (rand() % 2) - 1;
	//GLint randSign2 = (rand() % 2) - 1;



	//GLfloat random3 = random2 * randSign1;
	//GLfloat random4 = random2 * randSign2;


	//fprintf(stderr, "Respawaned a random at %d \n", (int) random);



	// Currently just place near the origin
	particle->offset = glm::vec3(random2, random3, random4);
	//particle.toWorld[3] = 1;
	particle->Color = glm::vec4(1.0f, 1.0f, 0.0f, 0.9f);
	particle->Life = life;
	particle->Velocity = glm::vec3(random5, random1, random6);

	//fprintf(stderr, "Respawaned a particle at %d %d %d %d \n", (int) particle.Position[0], (int) particle.Position[1], (int) particle.Position[2], (int) particle.Position[3]);
}

GLfloat ParticleGenerator::GetLife() {
	return this->life;
}

void ParticleGenerator::setCenter(glm::vec3 center) {
	this->center = center;
}


void ParticleGenerator::RenderFire(glm::mat4 toWorld) {
	Update();
	// Use additive blending to give it a 'glow' effect
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glUseProgram(shader);
	glEnable(GL_POINT_SPRITE);
	for (Particle* particle : this->particles)
	{
		if (particle->Life > 0.0f)
		{
			glm::mat4 loc = glm::translate(toWorld, particle->offset);
			GLuint uToWorld = glGetUniformLocation(shader, "toWorld");
			glUniformMatrix4fv(uToWorld, 1, GL_FALSE, &(loc[0][0]));
			GLuint uOffset = glGetUniformLocation(shader, "offset");
			glUniform3fv(uOffset, 1, (const GLfloat*)glm::value_ptr(particle->offset));
			GLint colorLoc = glGetUniformLocation(shader, "color");
			glUniform4fv(colorLoc, (GLsizei)1, (const GLfloat*)glm::value_ptr(particle->Color));
			GLuint uProjection = glGetUniformLocation(shader, "projection");
			glUniformMatrix4fv(uProjection, 1, GL_FALSE, &(Window::P[0][0]));
			GLuint uView = glGetUniformLocation(shader, "view");
			glUniformMatrix4fv(uView, 1, GL_FALSE, &(Window::V[0][0]));

			// For billboarding
			GLuint CameraRight_ID = glGetUniformLocation(shader, "cameraRight");
			GLuint CameraUp_ID = glGetUniformLocation(shader, "cameraUp");
			if (engine) {
				glUniform3fv(CameraRight_ID, 1, (const GLfloat*)glm::value_ptr(glm::vec3(1.0f, 0.0f, 0.0f)));
				glUniform3fv(CameraUp_ID, 1, (const GLfloat*)glm::value_ptr(glm::vec3(0.0f, 1.0f, 1.0f)));
			}
			else {
				glUniform3f(CameraRight_ID, Window::V[0][0], Window::V[1][0], Window::V[2][0]);
				glUniform3f(CameraUp_ID, Window::V[0][1], Window::V[1][1], Window::V[2][1]);
			}	
			// Bind texture
			glBindTexture(GL_TEXTURE_2D, texture);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texwidth, texheight, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
			glGenerateMipmap(GL_TEXTURE_2D);

			//Good pratice to free and unbind
			SOIL_free_image_data(image);
			glBindTexture(GL_TEXTURE_2D, 0);


			glBindVertexArray(this->VAO);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			//glDrawArrays(GL_TRIANGLES, 0, 6);
			glBindVertexArray(0);
		}
	}
	// Don't forget to reset to default blending mode
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void ParticleGenerator::RenderExplosion(glm::mat4 toWorld, GLint numParts) {
	GLfloat dt = (GetLife() / 25.0f);
	// Add new particles 
	for (GLint i = 0; i < numParts; ++i)
	{
		int unusedParticle = this->firstUnusedParticle();
		this->respawnParticleExplosion(this->particles[unusedParticle]);
	}
	// Update all particles
	for (GLuint i = 0; i < this->amount; ++i)
	{
		Particle* p = this->particles[i];
		p->Life -= dt; // reduce life
		if (p->Life > 0.0f)
		{	// particle is alive, thus update
			p->offset += p->Velocity * dt;

			// Check colored region
			if (p->Life / life < 0.8f)
				p->Color[1] = 0.5f;
			if (p->Life / life < 0.4f)
				p->Color[1] = 0.0f;
		}
	}

	// Use additive blending to give it a 'glow' effect
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glUseProgram(shader);
	glEnable(GL_POINT_SPRITE);
	for (Particle* particle : this->particles)
	{
		if (particle->Life > 0.0f)
		{
			
			glm::mat4 loc = glm::translate(toWorld, particle->offset);
			GLuint uToWorld = glGetUniformLocation(shader, "toWorld");
			glUniformMatrix4fv(uToWorld, 1, GL_FALSE, &(loc[0][0]));
			GLuint uOffset = glGetUniformLocation(shader, "offset");
			glUniform3fv(uOffset, 1, (const GLfloat*)glm::value_ptr(particle->offset));
			GLint colorLoc = glGetUniformLocation(shader, "color");
			glUniform4fv(colorLoc, (GLsizei)1, (const GLfloat*)glm::value_ptr(particle->Color));
			GLuint uProjection = glGetUniformLocation(shader, "projection");
			glUniformMatrix4fv(uProjection, 1, GL_FALSE, &(Window::P[0][0]));
			GLuint uView = glGetUniformLocation(shader, "view");
			glUniformMatrix4fv(uView, 1, GL_FALSE, &(Window::V[0][0]));

			// For billboarding
			GLuint CameraRight_ID = glGetUniformLocation(shader, "cameraRight");
			GLuint CameraUp_ID = glGetUniformLocation(shader, "cameraUp");
			glUniform3f(CameraRight_ID, 0.0f, 0.0f, 1.0f);
			glUniform3f(CameraUp_ID, 0.0f, 1.0f, 0.0f);
			//glUniform3f(CameraRight_ID, Window::V[0][0], Window::V[1][0], Window::V[2][0]);
			//glUniform3f(CameraUp_ID, Window::V[0][1], Window::V[1][1], Window::V[2][1]);

			// Bind texture
			glBindTexture(GL_TEXTURE_2D, texture);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texwidth, texheight, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
			glGenerateMipmap(GL_TEXTURE_2D);

			//Good pratice to free and unbind
			SOIL_free_image_data(image);
			glBindTexture(GL_TEXTURE_2D, 0);


			glBindVertexArray(this->VAO);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			//glDrawArrays(GL_TRIANGLES, 0, 6);
			glBindVertexArray(0);
		}
	}
	// Don't forget to reset to default blending mode
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}