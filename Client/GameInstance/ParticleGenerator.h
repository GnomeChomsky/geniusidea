#pragma once
/*******************************************************************
** This code is part of Breakout.
**
** Breakout is free software: you can redistribute it and/or modify
** it under the terms of the CC BY 4.0 license as published by
** Creative Commons, either version 4 of the License, or (at your
** option) any later version.
******************************************************************/
#ifndef PARTICLE_GENERATOR_H
#define PARTICLE_GENERATOR_H
#include <vector>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <Soil.h>
#include <glm/gtc/type_ptr.hpp>


#include "shader.h"
//#include "Mesh.h"
//#include "Ship.h"
//#include "texture.h"

// Represents a single particle and its state
struct Particle {
	glm::vec3 position, Velocity, offset;
	glm::vec4 Color;
	GLfloat Life;

	Particle() : offset(0.0f), Velocity(0.0f, 5.0f ,0.0f), Color(1.0f), Life(0.0f) { }
};


// ParticleGenerator acts as a container for rendering a large number of 
// particles by repeatedly spawning and updating particles and killing 
// them after a given amount of time.
class ParticleGenerator
{
public:
	// Constructor
	ParticleGenerator(GLint shader, GLuint amount, GLfloat life, GLuint newParticles, GLfloat size, GLfloat offset, GLfloat velocity, bool engine);
	// Update all particles
	void Update();
	// Render all particles
	void Draw();

	void RenderFire(glm::mat4 toWorld);
	void RenderExplosion(glm::mat4 toWorld, GLint numParts);

	GLfloat GetLife();
	void setCenter(glm::vec3 center);
	GLuint newParticles;


private:
	// State
	std::vector<Particle*> particles;
	GLuint amount;
	// Render state
	GLint shader;
	GLuint texture;
	glm::vec3 center;
	GLfloat life;
	GLfloat size;
	GLfloat velocity;
	GLfloat offset;
	bool engine;
	int texheight;
	int texwidth;
	unsigned char * image;
	GLuint VAO;
	// Initializes buffer and vertex attributes
	void init();
	// Returns the first Particle index that's currently unused e.g. Life <= 0.0f or 0 if no particle is currently inactive
	GLuint firstUnusedParticle();
	// Respawns particle
	void respawnParticle(Particle* particle);

	void respawnParticleExplosion(Particle* particle);
};

#endif