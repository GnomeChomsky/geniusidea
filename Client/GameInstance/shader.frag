#version 330 core

in vec3 fragVert;
in vec3 fragNormal;
in vec2 TexCoords;

out vec4 color;

struct Light {
	vec3 intensity;
	vec3 direction;
	float ambient;

	float theta;
	float cosExp;

	vec3 pos;
	float spec;
};

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
}; 

uniform Material material;
uniform Light light;
uniform vec3 cameraPos;
uniform sampler2D texture_diffuse1;

void main()
{

	vec4 texColor = texture(texture_diffuse1, TexCoords);

	// if there are no textures loaded for the fragment, load the objects color
	if (texColor.x == 0.0 && texColor.y == 0.0 && texColor.z == 0.0) {
		texColor = vec4(material.diffuse, 1.0f);
	}

	vec3 objco = vec3(texColor);

	//this block calculates the diffuse color
	vec3 norm = normalize(fragNormal);
	vec3 lightDir = normalize(light.pos - fragVert);
	float diff = max(dot(norm, lightDir), 0.0f);
	vec3 diffuseColor = diff * light.intensity;
	diffuseColor.x = diffuseColor.x * objco.x;
	diffuseColor.y = diffuseColor.y * objco.y;
	diffuseColor.z = diffuseColor.z * objco.z;

	//this block calculates ambient color
	vec3 ambColor = light.ambient * light.intensity;
	ambColor.x = ambColor.x * material.ambient.x;
	ambColor.y = ambColor.y * material.ambient.y;
	ambColor.z = ambColor.z * material.ambient.z;

	//this block calculates the spec color
	vec3 viewDir = normalize(cameraPos - fragVert);
	vec3 reflectDir = reflect(-lightDir, norm);
	float specul = pow(max(dot(viewDir, reflectDir), 0.0), 32);
	//vec3 specColor = light.spec * specul * light.intensity * material.shininess;
	vec3 specColor = vec3(0.0f,0.0f,0.0f);

	vec3 tempCo = (ambColor + diffuseColor + specColor);

	color = vec4(tempCo, 1.0f);
}