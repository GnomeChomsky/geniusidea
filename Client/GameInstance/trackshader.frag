#version 330
 
uniform sampler2D leftEyeTexture;
uniform sampler2D rightEyeTexture;

uniform int whichTexture;
 
in vec2 TexCoords;
 
out vec4 color;
 
void main(void)
{
	vec4 leftFrag = texture(leftEyeTexture, TexCoords);
	leftFrag = vec4(1.0, leftFrag.g, leftFrag.b, 1.0); // Left eye is full red and actual green and blue
	
	vec4 rightFrag = texture(rightEyeTexture, TexCoords);
	rightFrag = vec4(rightFrag.r, 1.0, 1.0, 1.0); // Right eye is full green and blue and actual red
 
	// Multiply left and right components for final output colour
	color = vec4(leftFrag.rgb * rightFrag.rgb, 1.0);


	//color = vec4(0.0f, TexCoords.x, TexCoords.y, 1.0f);
	if(whichTexture == 0)
		color = texture(leftEyeTexture, TexCoords);
	else if (whichTexture == 1)
		color = texture(rightEyeTexture, TexCoords);
	else
		color = vec4(leftFrag.rgb * rightFrag.rgb, 1.0);

}