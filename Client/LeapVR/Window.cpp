#include "Window.h"

const char* window_title = "Fruit Ninja VR";
GLint shaderProgram, quadShader;

int modelnum = 1;
int whichTex = 0;
float rotation = 0.0f;

Model* dojo;
Model* crate;
Model* apple;
Model* orange;
Model* pear;
Model* rhand;
Model* lhand;
Model* head;
Model* katana;

Drawable* Window::gameObjects[20];

std::vector<Drawable*> Window::objUpdates;

GLuint leftFramebuffer, rightFramebuffer, leftTexture, rightTexture;
unsigned int quadVAO, quadVBO;

Lights* light;

Model* Window::slice;
Cam* Window::camera;
bool cursorDisabled = true;
std::map <string, bool > key_press;

int Window::width;
int Window::height;

glm::mat4 Window::P;
glm::mat4 Window::V;

void Window::initialize_objects()
{
	// load game objects with nullptrs until the server populates the objects
	for (int i = 0; i < 20; i++) {
		gameObjects[i] = nullptr;
	}

	///////////////////////// generate frame buffers for the left and right eye
	glGenFramebuffers(1, &leftFramebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, leftFramebuffer);
	//bind left texture to left frame buffer
	glGenTextures(1, &leftTexture);
	glBindTexture(GL_TEXTURE_2D, leftTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, leftTexture, 0);

	unsigned int rbo;
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height); // use a single renderbuffer object for both a depth AND stencil buffer.
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo); // now actually attach it
																								  // now that we actually created the framebuffer and added all attachments we want to check if it is actually complete now
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		cout << "ERROR::FRAMEBUFFER:: Left Framebuffer is not complete!" << endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	glGenFramebuffers(1, &rightFramebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, rightFramebuffer);
	//bind left texture to left frame buffer
	glGenTextures(1, &rightTexture);
	glBindTexture(GL_TEXTURE_2D, rightTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, rightTexture, 0);

	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height); // use a single renderbuffer object for both a depth AND stencil buffer.
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo); // now actually attach it
																								  // now that we actually created the framebuffer and added all attachments we want to check if it is actually complete now
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		cout << "ERROR::FRAMEBUFFER:: Right Framebuffer is not complete!" << endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	/////////////////////// Done generating frame buffers

	// Construct screen-spanning quad to render anaglyph stereo to
	float quadVertices[] = { // vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
							 // positions   // texCoords
		-1.0f,  1.0f,  0.0f, 1.0f,
		-1.0f, -1.0f,  0.0f, 0.0f,
		1.0f, -1.0f,  1.0f, 0.0f,

		-1.0f,  1.0f,  0.0f, 1.0f,
		1.0f, -1.0f,  1.0f, 0.0f,
		1.0f,  1.0f,  1.0f, 1.0f
	};
	// screen quad VAO
	glGenVertexArrays(1, &quadVAO);
	glGenBuffers(1, &quadVBO);
	glBindVertexArray(quadVAO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
	// Done constructing quad

	camera = new Cam();

	// Load the shader program. Make sure you have the correct filepath up top
	shaderProgram = LoadShaders("../GameInstance/shader.vert", "../GameInstance/shader.frag");
	quadShader = LoadShaders("../GameInstance/trackshader.vert", "../GameInstance/trackshader.frag");

	dojo = new Model("../models/dojo2/dojo2.obj");
	crate = new Model("../models/woodencrate/WoodenCrate.obj");
	apple = new Model("../models/apple/apple.obj");
	rhand = new Model("../models/rhand/rhand.obj");
	lhand = new Model("../models/rhand/rhand.obj", true);
	orange = new Model("../models/orange/orange.obj");
	pear = new Model("../models/pear/pear.obj");
	head = new Model("../models/head/Casque-Samurai-4.obj");
	katana = new Model("../models/katana/katana.obj");
	slice = new Model("../models/explosion.obj");

	light = new Lights(1);
	light->position = glm::vec3(0.0f, 5.0f, -6.75f);
}

void Window::processPacket(Packet packet) {
	if (packet.objID >= 20) {
		printf("INVALID OBJECT ID: %d\n", packet.objID);
		return;
	}
	if (gameObjects[packet.objID] == nullptr) {
		gameObjects[packet.objID] = new Drawable(packet, shaderProgram);
		printf("New Object %d centered at (%f,%f,%f)\n", packet.objID, gameObjects[packet.objID]->position.x, gameObjects[packet.objID]->position.y, gameObjects[packet.objID]->position.z);
		switch (packet.objID) {
		case 0: // DOJO
			gameObjects[packet.objID]->model = dojo;
			break;
		case 1: // DOJO MIRROR
			gameObjects[packet.objID]->model = dojo;
			break;
		case 2: // Crate
			gameObjects[packet.objID]->model = crate;
			break;
		case 3: // Oculus Head
			gameObjects[packet.objID]->model = head;
			break;
		case 4: // Oculus Hand1
			gameObjects[packet.objID]->model = rhand;
			break;
		case 5: // Oculus Hand2
			gameObjects[packet.objID]->model = lhand;
			break;
		case 6: // Leap Head
			gameObjects[packet.objID]->model = head;
			break;
		case 7: // Leap Hand
			gameObjects[packet.objID]->model = katana;
			break;
		case 8: // Apple1
			gameObjects[packet.objID]->model = apple;
			break;
		case 9: // Apple2
			gameObjects[packet.objID]->model = apple;
			break;
		case 10: // Apple3
			gameObjects[packet.objID]->model = apple;
			break;
		case 11: // Apple4
			gameObjects[packet.objID]->model = apple;
			break;
		case 12: // Orange1
			gameObjects[packet.objID]->model = orange;
			break;
		case 13: // Orange2
			gameObjects[packet.objID]->model = orange;
			break;
		case 14: // Orange3
			gameObjects[packet.objID]->model = orange;
			break;
		case 15: // Orange4
			gameObjects[packet.objID]->model = orange;
			break;
		case 16: // Pear1
			gameObjects[packet.objID]->model = pear;
			break;
		case 17: // Pear2
			gameObjects[packet.objID]->model = pear;
			break;
		case 18: // Pear3
			gameObjects[packet.objID]->model = pear;
			break;
		case 19: // Pear4
			gameObjects[packet.objID]->model = pear;
			break;
		default:
			gameObjects[packet.objID]->model = apple;
			break;
		}
	}
	else {
		gameObjects[packet.objID]->update(packet);
	}
}

// Treat this as a destructor function. Delete dynamically allocated memory here.
void Window::clean_up()
{
	glDeleteProgram(shaderProgram);
}

GLFWwindow* Window::create_window(int width, int height)
{
	// Initialize GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		return NULL;
	}

	// 4x antialiasing
	glfwWindowHint(GLFW_SAMPLES, 4);

#ifdef __APPLE__ // Because Apple hates comforming to standards
	// Ensure that minimum OpenGL version is 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	// Enable forward compatibility and allow a modern OpenGL context
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	// Create the GLFW window
	GLFWwindow* window = glfwCreateWindow(width, height, window_title, NULL, NULL);

	// Check if the window could not be created
	if (!window)
	{
		fprintf(stderr, "Failed to open GLFW window.\n");
		fprintf(stderr, "Either GLFW is not installed or your graphics card does not support modern OpenGL.\n");
		glfwTerminate();
		return NULL;
	}

	// Make the context of the window
	glfwMakeContextCurrent(window);

	// Set swap interval to 1
	glfwSwapInterval(1);

	// Get the width and height of the framebuffer to properly resize the window
	glfwGetFramebufferSize(window, &width, &height);
	// Call the resize callback to make sure things get drawn immediately
	Window::resize_callback(window, width, height);

	return window;
}

void Window::resize_callback(GLFWwindow* window, int width, int height)
{
	Window::width = width;
	Window::height = height;
	// Set the viewport size. This is the only matrix that OpenGL maintains for us in modern OpenGL!
	glViewport(0, 0, width, height);

	if (height > 0)
	{
		P = glm::perspective(45.0f, (float)width / (float)height, 0.1f, 50000.0f);
		if (camera)
			V = glm::lookAt(camera->get_pos(), camera->get_look_at(), camera->get_up());
		else
			V = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	}
}

void Window::idle_callback()
{
	if (key_press["w"]) { camera->translate(0); }
	if (key_press["a"]) { camera->translate(1); }
	if (key_press["s"]) { camera->translate(2); }
	if (key_press["d"]) { camera->translate(3); }
	if (key_press["right"]) {
		rotation += 2.0f;
		if (rotation == 360.0f) rotation = 0.0f;
		if (gameObjects[1] != nullptr) {
			gameObjects[1]->rotation = glm::rotate(glm::mat4(1.0f), glm::radians(rotation), glm::vec3(0.0f, 1.0f, 0.0f));
		}
	}
	if (key_press["left"]) {
		rotation -= 2.0f;
		if (rotation == 360.0f) rotation = 0.0f;
		if (gameObjects[1] != nullptr) {
			gameObjects[1]->rotation = glm::rotate(glm::mat4(1.0f), glm::radians(rotation), glm::vec3(0.0f, 1.0f, 0.0f));
		}
	}

	if (key_press["left"] || key_press["right"]) {
		std::cout << rotation << std::endl;
		objUpdates.push_back(gameObjects[1]);
	}

	//V = glm::lookAt(camera->get_pos(), camera->get_look_at(), camera->get_up());

}

void Window::display_callback()
{
	glm::vec3 left_right = 0.01f * glm::normalize(glm::cross(camera->get_direction(), camera->get_up()));
	glm::vec3 cam_poses[] = { -1.0f * left_right,  left_right };
	GLuint buffers[] = {leftFramebuffer, rightFramebuffer};

	// Draw to left and right eye framebuffer
	for (int i = 0; i < 2; i++) {
		glBindFramebuffer(GL_FRAMEBUFFER, buffers[i]);
		P = glm::perspective(45.0f, (float)width / (float)height, 0.1f, 50000.0f);
		V = glm::lookAt(camera->get_pos() + cam_poses[i], camera->get_look_at(), camera->get_up());

		// Clear the color and depth buffers
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use the shader of programID
		glUseProgram(shaderProgram);

		GLuint uProjection = glGetUniformLocation(shaderProgram, "projection");
		GLuint uModelView = glGetUniformLocation(shaderProgram, "view");

		glUniformMatrix4fv(uProjection, 1, GL_FALSE, glm::value_ptr(P));
		glUniformMatrix4fv(uModelView, 1, GL_FALSE, glm::value_ptr(V));

		// lighting block
		GLuint MatrixID = glGetUniformLocation(shaderProgram, "light.intensity");
		glUniform3f(MatrixID, (light->intensity.x), (light->intensity.y), (light->intensity.z));

		MatrixID = glGetUniformLocation(shaderProgram, "light.direction");
		glUniform3f(MatrixID, (light->direction.x), (light->direction.y), (light->direction.z));

		MatrixID = glGetUniformLocation(shaderProgram, "light.ambient");
		glUniform1f(MatrixID, light->ambient);

		MatrixID = glGetUniformLocation(shaderProgram, "light.pos");
		glUniform3f(MatrixID, light->position.x, light->position.y, light->position.z);

		MatrixID = glGetUniformLocation(shaderProgram, "light.spec");
		glUniform1f(MatrixID, light->specular);

		MatrixID = glGetUniformLocation(shaderProgram, "light.theta");
		glUniform1f(MatrixID, light->theta);

		MatrixID = glGetUniformLocation(shaderProgram, "light.cosExp");
		glUniform1f(MatrixID, light->cosExp);

		MatrixID = glGetUniformLocation(shaderProgram, "cameraPos");
		glUniform3f(MatrixID, cam_poses[i].x, cam_poses[i].y, cam_poses[i].z);

		for (int i = 0; i < 20; i++) {
			if (gameObjects[i] != nullptr && i != 6) {
				if (gameObjects[i]->enabled)
					gameObjects[i]->draw();
				else {
					slice->toWorld = glm::translate(glm::mat4(), gameObjects[i]->position) * gameObjects[i]->rotation * glm::scale(glm::mat4(), glm::vec3(0.00075f)); // T * R * S
					slice->Draw(shaderProgram);
				}
			}
		}
	}

	// now that the frame buffers are drawn to, use the anaglyph shader to draw the scene
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	// Clear the color and depth buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(quadShader);
	glBindVertexArray(quadVAO);

	GLuint texLoc = glGetUniformLocation(quadShader, "whichTexture");
	glUniform1i(texLoc, whichTex);

	texLoc = glGetUniformLocation(quadShader, "leftEyeTexture");
	glUniform1i(texLoc, 0);

	texLoc = glGetUniformLocation(quadShader, "rightEyeTexture");
	glUniform1i(texLoc, 1);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, leftTexture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, rightTexture);

	glDrawArrays(GL_TRIANGLES, 0, 6);


}

void Window::cursor_pos_callback(GLFWwindow* window, double xpos, double ypos)
{
	glm::vec3 mousePos;
	float d;

	if (!glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) && !glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) && !cursorDisabled)
	{
		// manipulate the camera
		mousePos.x = ((2.0f * (float)xpos) - Window::width) / Window::width;
		mousePos.y = (Window::height - (2.0f * (float)ypos)) / Window::height;
		mousePos.z = 0.0f;
		d = length(mousePos);
		d = (d < 1.0f) ? d : 1.0f;

		if (glm::isnan(mousePos.x) || glm::isnan(mousePos.y) || glm::isnan(mousePos.z)) {
			std::cout << "ISNAN !!! \n";
			return;
		}

		camera->moveVV(mousePos);


	}

}

void Window::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	// Check for a key press
	if (action == GLFW_PRESS)
	{
		// Check if escape was pressed
		switch (key)
		{
		case GLFW_KEY_GRAVE_ACCENT: glfwSetWindowShouldClose(window, GL_TRUE); break;
		case GLFW_KEY_ESCAPE:
			if (cursorDisabled == false) {
				glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			}
			else {
				glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			}
			cursorDisabled = !cursorDisabled;
			break;

		case GLFW_KEY_W: key_press["w"] = true; break;
		case GLFW_KEY_A: key_press["a"] = true; break;
		case GLFW_KEY_S: key_press["s"] = true; break;
		case GLFW_KEY_D: key_press["d"] = true; break;
		case GLFW_KEY_F: modelnum++; if (modelnum == 20) modelnum = 1; break;
		case GLFW_KEY_T: whichTex++;  if (whichTex == 3) whichTex = 0; break;
		case GLFW_KEY_UP: key_press["up"] = true; break;
		case GLFW_KEY_DOWN: key_press["down"] = true; break;
		case GLFW_KEY_RIGHT: key_press["right"] = true; break;
		case GLFW_KEY_LEFT: key_press["left"] = true; break;
		case GLFW_KEY_F1: key_press["F1"] = true; break;
		case GLFW_KEY_F2: key_press["F2"] = true; break;
		case GLFW_KEY_F3: key_press["F3"] = true; break;
		case GLFW_KEY_ENTER: push_to_updates(nullptr); break;
		}
	}
	else if (action == GLFW_RELEASE)
	{
		switch (key)
		{
		case GLFW_KEY_W: key_press["w"] = false; break;
		case GLFW_KEY_A: key_press["a"] = false; break;
		case GLFW_KEY_S: key_press["s"] = false; break;
		case GLFW_KEY_D: key_press["d"] = false; break;
		case GLFW_KEY_F: key_press["f"] = false; break;
		case GLFW_KEY_UP: key_press["up"] = false; break;
		case GLFW_KEY_DOWN: key_press["down"] = false; break;
		case GLFW_KEY_RIGHT: key_press["right"] = false; break;
		case GLFW_KEY_LEFT: key_press["left"] = false; break;
		case GLFW_KEY_F1: key_press["F1"] = false; break;
		case GLFW_KEY_F2: key_press["F2"] = false; break;
		case GLFW_KEY_F3: key_press["F3"] = false; break;
		}
	}

}

void Window::mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		key_press["l_click"] = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
		key_press["l_click"] = false;
	}
}

void Window::scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
	light->spotWiden((float)yoffset);
}

void Window::push_to_updates(Drawable * object) {
	if (std::find(objUpdates.begin(), objUpdates.end(), object) == objUpdates.end()) {
		objUpdates.push_back(object);
	}
}