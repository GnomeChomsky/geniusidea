// main.cpp : Defines the entry point for the console application.
//

#include "main.h"
#include <Leap.h>
#include "Window.h"

using namespace Leap;

ClientGame * client;
GLFWwindow* window;

Leap::Hand handList[2];
Leap::Vector handPositions[2];

Leap::Frame currFrame, prevFrame;
glm::mat4 prevRot, currRot;
int index = 0;

void error_callback(int error, const char* description)
{
	// Print error
	fputs(description, stderr);
}

void setup_callbacks()
{
	// Set the error callback
	glfwSetErrorCallback(error_callback);
	// Set the key callback
	glfwSetKeyCallback(window, Window::key_callback);
	//glfwSetInputMode(window, GLFW_STICKY_MOUSE_BUTTONS, 1);
	glfwSetCursorPosCallback(window, Window::cursor_pos_callback);
	// W:  set mouse scroll callback
	glfwSetScrollCallback(window, Window::scroll_callback);
	// Set the window resize callback
	glfwSetFramebufferSizeCallback(window, Window::resize_callback);
	glfwSetMouseButtonCallback(window, Window::mouse_button_callback);
}

void setup_glew()
{
	// Initialize GLEW. Not needed on OSX systems.
#ifndef __APPLE__
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		glfwTerminate();
	}
	fprintf(stdout, "Current GLEW version: %s\n", glewGetString(GLEW_VERSION));
#endif
}

void setup_opengl_settings()
{
#ifndef __APPLE__
	// Setup GLEW. Don't do this on OSX systems.
	setup_glew();
#endif
	// Enable depth buffering
	glEnable(GL_DEPTH_TEST);
	// Related to shaders and z value comparisons for the depth buffer
	glDepthFunc(GL_LEQUAL);
	// Set polygon drawing mode to fill front and back of each polygon
	// You can also use the paramter of GL_LINE instead of GL_FILL to see wireframes
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	// Disable backface culling to render both sides of polygons
	glDisable(GL_CULL_FACE);
	// Set clear color
	glClearColor(0.05f, 0.8f, 0.85f, 1.0f);
}

void print_versions()
{
	// Get info of GPU and supported OpenGL version
	printf("Renderer: %s\n", glGetString(GL_RENDERER));
	printf("OpenGL version supported %s\n", glGetString(GL_VERSION));

	//If the shading language symbol is defined
#ifdef GL_SHADING_LANGUAGE_VERSION
	std::printf("Supported GLSL version is %s.\n", (char *)glGetString(GL_SHADING_LANGUAGE_VERSION));
#endif
}

class LeapEventListener : public Listener {
public:
	virtual void onConnect(const Controller&);
	virtual void onDisconnect(const Controller&);
	virtual void onFrame(const Controller&);
};

void LeapEventListener::onConnect(const Controller& controller) {
	std::cout << "Connected" << std::endl;
	// Enable gestures, set Config values:
	controller.enableGesture(Gesture::TYPE_SWIPE);
	controller.config().setFloat("Gesture.Swipe.MinLength", 200.0);
	controller.config().save();
}

//Not dispatched when running in a debugger
void LeapEventListener::onDisconnect(const Controller& controller) {
	std::cout << "Disconnected" << std::endl;
}

glm::mat4 makeRotation(Leap::Vector handDir) {
	glm::mat4 xToWorld, yToWorld;

	glm::vec3 direction(handDir[0], handDir[1], handDir[2]);
	glm::vec3 yplane(0.0f, 1.0f, 0.0f);


	glm::vec4 direction_homog(direction, 1.0f);
	float rotAngle = 0.0f;

	// find rotation angle in y axis
	rotAngle = -1.0f * handDir.angleTo(Leap::Vector(0.0f,1.0f,0.0f));
	glm::vec3 crossr = glm::cross(yplane, direction);
	yToWorld = glm::rotate(glm::mat4(), rotAngle, crossr);

	xToWorld = glm::mat4();

	return xToWorld * yToWorld;
}

void LeapEventListener::onFrame(const Controller& controller) {
	prevFrame = currFrame;
	currFrame = controller.frame();

	Leap::HandList hands = currFrame.hands();
	if (hands.leftmost().isLeft()) {
		handList[0] = hands.leftmost(); // left hand
		handList[1] = hands.rightmost(); // right hand
	}
	else {
		handList[0] = hands.rightmost();
		handList[1] = hands.leftmost();
	}

	handPositions[0] = handList[0].palmPosition();
	handPositions[1] = handList[1].palmPosition();

	glm::mat4 rot = makeRotation(handList[0].direction());

	Drawable* temp = Window::gameObjects[7];

	if (temp != nullptr) {
		glm::vec4 pos_homog(glm::vec3(handPositions[1].x * .001f, (handPositions[1].y * .001f) - 0.5f, (handPositions[1].z * .001f) - 2.0f), 1.0f);
		glm::mat4 rotation = glm::rotate(glm::mat4(), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		pos_homog = rotation * pos_homog;
		temp->position = Window::camera->get_pos() + glm::vec3(pos_homog);
		temp->rotation = rot;
		Window::push_to_updates(temp);


		glm::vec4 dir(handList[0].direction()[0], handList[0].direction()[1], handList[0].direction()[2], 1.0f);
		glm::mat4 rotat = glm::rotate(glm::mat4(), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		dir = rotat * dir;
		glm::vec3 undir(dir);

		// detect pt line collision
		glm::vec3 x1 = temp->position;
		glm::vec3 x2 = x1 + undir;
		glm::vec3 x0, line10;
		glm::vec3 line21 = undir;
		float d;
		Drawable* tempFruit;
		for (int i = 8; i < 20; i++) {
			tempFruit = Window::gameObjects[i];
			if (tempFruit != nullptr && tempFruit->enabled) {
				x0 = glm::vec3(tempFruit->position);
				line10 = x1 - x0;
				d = glm::length(glm::cross(line21, line10)) / glm::length(line21);
				if (d < 0.25f && glm::abs(glm::length(tempFruit->position - temp->position)) < 2.0f) {
					printf("KABOOM! %d\n", tempFruit->objectID);
					tempFruit->enabled = false;
					Window::push_to_updates(tempFruit);
				}

			}
		}



	}


	

}

void detectCollision () {
	for (int i = 0; i < 2; i++) {
		glm::vec3 currHandPos = glm::vec3(handPositions[i].x, handPositions[i].y, handPositions[i].z);
		for (int j = 0; j < 20; j++) {
			Drawable* temp = Window::gameObjects[j];
			float dist = glm::length(currHandPos - temp->position);
			std::cout << "DIST: " << dist << std::endl;
			if (dist < 1.0f) {
				temp->enabled = false;
			}
		}
	}
}

int main()
{

	Controller controller;
	LeapEventListener listener;
	controller.addListener(listener);

	// initialize the client 
	client = new ClientGame();

	// Create the GLFW window
	window = Window::create_window(1920, 1080);
	// Print OpenGL and GLSL versions
	print_versions();
	// Setup callbacks
	setup_callbacks();
	// Setup OpenGL settings, including lighting, materials, etc.
	setup_opengl_settings();
	// Initialize objects/pointers for rendering
	Window::initialize_objects();


	// Loop while GLFW window should stay open
	while (!glfwWindowShouldClose(window))
	{

		Window::objUpdates.clear();
		//do game stuff
		client->update();

		// Main render display callback. Rendering of objects is done here.
		Window::display_callback();

		// Gets events, including input such as keyboard and mouse or window resizing
		glfwPollEvents();
		// Swap buffers
		glfwSwapBuffers(window);

		// Idle callback. Updating objects, etc. can be done here.
		Window::idle_callback();

		client->sendActionPackets();

	}

	Window::clean_up();
	// Destroy the window
	glfwDestroyWindow(window);
	// Terminate GLFW
	glfwTerminate();

	exit(EXIT_SUCCESS);
}

