#include "ClientGame.h"

ClientGame::ClientGame(void)
{

    network = new ClientNetwork();
	fprintf(stderr, "CLIENT CREATED\n");
	fprintf(stdout, "Packet size: %d\n", sizeof(Packet));
    // send init packet
    const unsigned int packet_size = sizeof(Packet);
    char packet_data[packet_size];

    Packet packet;
    packet.packet_type = INIT_CONNECTION;

    packet.serialize(packet_data);

    NetworkServices::sendMessage(network->ConnectSocket, packet_data, packet_size);
}


ClientGame::~ClientGame(void)
{
}

void ClientGame::sendActionPackets()
{
	if (Window::objUpdates.size() == 0) return;
	// send action packet
	const unsigned int packet_size = Window::objUpdates.size() * sizeof(Packet);
    char packet_data[20 * sizeof(Packet)];

	for (int i = 0; i < Window::objUpdates.size(); i++) {
		Window::objUpdates[i]->convertToPacket().serialize(&(packet_data[i * sizeof(Packet)]));
	}

    NetworkServices::sendMessage(network->ConnectSocket, packet_data, packet_size);
}

void ClientGame::update()
{
    Packet packet;
    int data_length = network->receivePackets(network_data);
    if (data_length <= 0) 
    {
        //no data recieved
        return;
    }
	//if (data_length != 2080) {
	//	printf("Packet lost in translation\n");
	//	sendActionPackets();
	//	return;
	//}
    unsigned int i = 0;
	int pack_num = 0;
    while (i < (unsigned int)data_length) 
    {
        packet.deserialize(&(network_data[i]));
        i += sizeof(Packet);
		pack_num++;
        switch (packet.packet_type) {
            case ACTION_EVENT:
				//printf("client received action event packet from server\n");
				Window::processPacket(packet);
                break;

            default:

                printf("error in packet types (type: %d)\n",packet.packet_type);

                break;
        }
    }
	//printf("NUMBER OF PACKETS RECEIVED: %d\n", pack_num);
}
