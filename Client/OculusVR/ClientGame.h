#pragma once
#include <winsock2.h>
#include "../../Network/ClientNetwork.h"
#include "Window.h"
#include "../../Network/StdAfx.h"

class ClientGame
{
public:
	ClientGame(void);
	~ClientGame(void);

	ClientNetwork* network;

	void sendActionPackets();

    char network_data[MAX_PACKET_SIZE];

    void update();
};

