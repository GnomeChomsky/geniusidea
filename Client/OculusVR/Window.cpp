#include "Window.h"

GLuint Window::shaderProgram;
//GLuint Window::trackProgram;

Model* dojo;
Model* crate;
Model* Window::apple;
Model* Window::orange;
Model* Window::pear;
Model* Window::slice;
Model* rhand;
Model* lhand;
Model* katana;
Model* head;

Drawable* Window::gameObjects[20];
std::vector<Drawable*> Window::objUpdates;

Lights* Window::light;
vector<Remote*> Window::remotes;

clock_t Window::begin_time;
bool Window::endState;

void Window::initialize(ovrSession& _session) {

	shaderProgram = LoadShaders("../GameInstance/shader.vert", "../GameInstance/shader.frag");

	glClearColor(0.0f, 0.5f, 1.0f, 0.0f);
	glEnable(GL_DEPTH_TEST);
	ovr_RecenterTrackingOrigin(_session);
	endState = false;

	remotes.push_back(new Remote());
	remotes.push_back(new Remote());

	dojo = new Model("../models/dojo2/dojo2.obj");
	crate = new Model("../models/woodencrate/WoodenCrate.obj");
	rhand = new Model("../models/rhand/rhand.obj");
	lhand = new Model("../models/rhand/rhand.obj", true);
	apple = new Model("../models/apple/apple.obj");
	orange = new Model("../models/orange/orange.obj");
	pear = new Model("../models/pear/pear.obj");
	head = new Model("../models/head/Casque-Samurai-4.obj");
	katana = new Model("../models/katana/katana.obj");
	slice = new Model("../models/explosion.obj");


	light = new Lights(2);
}

void Window::processPacket(Packet packet) {
	if (packet.objID >= 20) {
		printf("INVALID OBJECT ID: %d\n", packet.objID);
		return;
	}
	if (gameObjects[packet.objID] == nullptr) {
		gameObjects[packet.objID] = new Drawable(packet, shaderProgram);
		printf("New Object %d centered at (%f,%f,%f)\n", packet.objID, gameObjects[packet.objID]->position.x, gameObjects[packet.objID]->position.y, gameObjects[packet.objID]->position.z);
		switch (packet.objID) {
		case 0: // DOJO
			gameObjects[packet.objID]->model = dojo;
			break;
		case 1: // DOJO MIRROR
			gameObjects[packet.objID]->model = dojo;
			break;
		case 2: // Crate
			gameObjects[packet.objID]->model = crate;
			break;
		case 3: // Oculus Head
			gameObjects[packet.objID]->model = head;
			break;
		case 4: // Oculus Hand1
			gameObjects[packet.objID]->model = rhand;
			break;
		case 5: // Oculus Hand2
			gameObjects[packet.objID]->model = lhand;
			break;
		case 6: // Leap Head
			gameObjects[packet.objID]->model = head;
			break;
		case 7: // Leap Hand
			gameObjects[packet.objID]->model = katana;
			break;
		case 8: // Apple1
			gameObjects[packet.objID]->model = apple;
			break;
		case 9: // Apple2
			gameObjects[packet.objID]->model = apple;
			break;
		case 10: // Apple3
			gameObjects[packet.objID]->model = apple;
			break;
		case 11: // Apple4
			gameObjects[packet.objID]->model = apple;
			break;
		case 12: // Orange1
			gameObjects[packet.objID]->model = orange;
			break;
		case 13: // Orange2
			gameObjects[packet.objID]->model = orange;
			break;
		case 14: // Orange3
			gameObjects[packet.objID]->model = orange;
			break;
		case 15: // Orange4
			gameObjects[packet.objID]->model = orange;
			break;
		case 16: // Pear1
			gameObjects[packet.objID]->model = pear;
			break;
		case 17: // Pear2
			gameObjects[packet.objID]->model = pear;
			break;
		case 18: // Pear3
			gameObjects[packet.objID]->model = pear;
			break;
		case 19: // Pear4
			gameObjects[packet.objID]->model = pear;
			break;
		default:
			gameObjects[packet.objID]->model = apple;
			break;
		}
	}
	else {
		gameObjects[packet.objID]->update(packet);
	}
}

void Window::reset(ovrSession& _session) {

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	ovr_RecenterTrackingOrigin(_session);
}

void Window::displayCallback(const glm::mat4 & projection, const glm::mat4 & headPose, ovrSession& _session, unsigned int& frame, int mode) {

	glUseProgram(shaderProgram);

	GLuint uProjection = glGetUniformLocation(shaderProgram, "projection");
	GLuint uModelView = glGetUniformLocation(shaderProgram, "view");

	glUniformMatrix4fv(uProjection, 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(uModelView, 1, GL_FALSE, glm::value_ptr(glm::inverse(headPose)));

	GLuint MatrixID = glGetUniformLocation(shaderProgram, "light.intensity");
	glUniform3f(MatrixID, (light->intensity.x), (light->intensity.y), (light->intensity.z));

	MatrixID = glGetUniformLocation(shaderProgram, "light.direction");
	glUniform3f(MatrixID, (light->direction.x), (light->direction.y), (light->direction.z));

	MatrixID = glGetUniformLocation(shaderProgram, "light.ambient");
	glUniform1f(MatrixID, light->ambient);

	MatrixID = glGetUniformLocation(shaderProgram, "light.pos");
	glUniform3f(MatrixID, light->position.x, light->position.y, light->position.z);

	MatrixID = glGetUniformLocation(shaderProgram, "light.spec");
	glUniform1f(MatrixID, light->specular);

	MatrixID = glGetUniformLocation(shaderProgram, "light.theta");
	glUniform1f(MatrixID, light->theta);

	MatrixID = glGetUniformLocation(shaderProgram, "light.cosExp");
	glUniform1f(MatrixID, light->cosExp);

	MatrixID = glGetUniformLocation(shaderProgram, "cameraPos");
	glUniform3f(MatrixID, (0.0f), (0.0f), (0.0f));

	for (int i = 0; i < 20; i++) {
		if (gameObjects[i] != nullptr && i != 3) {
			if (gameObjects[i]->enabled)
				gameObjects[i]->draw();
			else {
				slice->toWorld = glm::translate(glm::mat4(), gameObjects[i]->position) * gameObjects[i]->rotation * glm::scale(glm::mat4(), glm::vec3(0.00075f)); // T * R * S
				slice->Draw(shaderProgram);
			}
		}
	}
}

Drawable* Window::detectCollision_fruit(Drawable* hand) {
	Drawable* toRet = NULL;

	float minDist = 1000.0f;

	for (int i = 8; i < 20; i++) {
		Drawable* temp = gameObjects[i];
		float tempDist = glm::length(temp->position - hand->position);

		if (temp != hand && tempDist < minDist) {
			minDist = tempDist;
			//sprintf_s(buff, "MIN DIST: (%f)\n", minDist);
			//OutputDebugStringA(buff);
			if (minDist < 0.35f) {
				toRet = temp;
			}
		}
	}
	return toRet;
}

void Window::push_to_updates(Drawable * object) {
	if (std::find(objUpdates.begin(), objUpdates.end(), object) == objUpdates.end()) {
		objUpdates.push_back(object);
	}
}
