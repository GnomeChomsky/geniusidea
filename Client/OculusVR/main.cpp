// main.cpp : Defines the entry point for the console application.
//

#include "main.h"

//void clientLoop(void *);
//
//ClientGame * client;
//
//int main()
//{
//	// initialize the client 
//	client = new ClientGame();
//
//	clientLoop((void*)12);
//}
//
//void clientLoop(void* arg)
//{
//    while(true)
//    {
//        //do game stuff
//        client->update();
//    }
//}

/************************************************************************************

Authors     :   Bradley Austin Davis <bdavis@saintandreas.org>
Copyright   :   Copyright Brad Davis. All Rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

************************************************************************************/


#define __STDC_FORMAT_MACROS 1

#define FAIL(X) throw std::runtime_error(X)


///////////////////////////////////////////////////////////////////////////////
//
// GLM is a C++ math library meant to mirror the syntax of GLSL 
//

// Import the most commonly used types into the default namespace
using glm::ivec3;
using glm::ivec2;
using glm::uvec2;
using glm::mat3;
using glm::mat4;
using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::quat;

///////////////////////////////////////////////////////////////////////////////
//
// GLEW gives cross platform access to OpenGL 3.x+ functionality.  
//

#include <GL/glew.h>

bool checkFramebufferStatus(GLenum target = GL_FRAMEBUFFER) {
	GLuint status = glCheckFramebufferStatus(target);
	switch (status) {
	case GL_FRAMEBUFFER_COMPLETE:
		return true;
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
		std::cerr << "framebuffer incomplete attachment" << std::endl;
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
		std::cerr << "framebuffer missing attachment" << std::endl;
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
		std::cerr << "framebuffer incomplete draw buffer" << std::endl;
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
		std::cerr << "framebuffer incomplete read buffer" << std::endl;
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
		std::cerr << "framebuffer incomplete multisample" << std::endl;
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
		std::cerr << "framebuffer incomplete layer targets" << std::endl;
		break;

	case GL_FRAMEBUFFER_UNSUPPORTED:
		std::cerr << "framebuffer unsupported internal format or image" << std::endl;
		break;

	default:
		std::cerr << "other framebuffer error" << std::endl;
		break;
	}

	return false;
}

bool checkGlError() {
	GLenum error = glGetError();
	if (!error) {
		return false;
	}
	else {
		switch (error) {
		case GL_INVALID_ENUM:
			std::cerr << ": An unacceptable value is specified for an enumerated argument.The offending command is ignored and has no other side effect than to set the error flag.";
			break;
		case GL_INVALID_VALUE:
			std::cerr << ": A numeric argument is out of range.The offending command is ignored and has no other side effect than to set the error flag";
			break;
		case GL_INVALID_OPERATION:
			std::cerr << ": The specified operation is not allowed in the current state.The offending command is ignored and has no other side effect than to set the error flag..";
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			std::cerr << ": The framebuffer object is not complete.The offending command is ignored and has no other side effect than to set the error flag.";
			break;
		case GL_OUT_OF_MEMORY:
			std::cerr << ": There is not enough memory left to execute the command.The state of the GL is undefined, except for the state of the error flags, after this error is recorded.";
			break;
		case GL_STACK_UNDERFLOW:
			std::cerr << ": An attempt has been made to perform an operation that would cause an internal stack to underflow.";
			break;
		case GL_STACK_OVERFLOW:
			std::cerr << ": An attempt has been made to perform an operation that would cause an internal stack to overflow.";
			break;
		}
		return true;
	}
}

void glDebugCallbackHandler(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *msg, GLvoid* data) {
	OutputDebugStringA(msg);
	std::cout << "debug call: " << msg << std::endl;
}

//////////////////////////////////////////////////////////////////////
//
// GLFW provides cross platform window creation
//

namespace glfw {
	inline GLFWwindow * createWindow(const uvec2 & size, const ivec2 & position = ivec2(INT_MIN)) {
		GLFWwindow * window = glfwCreateWindow(size.x, size.y, "glfw", nullptr, nullptr);
		if (!window) {
			FAIL("Unable to create rendering window");
		}
		if ((position.x > INT_MIN) && (position.y > INT_MIN)) {
			glfwSetWindowPos(window, position.x, position.y);
		}
		return window;
	}
}

// A class to encapsulate using GLFW to handle input and render a scene
class GlfwApp {

protected:
	uvec2 windowSize;
	ivec2 windowPosition;
	GLFWwindow * window{ nullptr };
	unsigned int frame{ 0 };

public:
	GlfwApp() {
		// Initialize the GLFW system for creating and positioning windows
		if (!glfwInit()) {
			FAIL("Failed to initialize GLFW");
		}
		glfwSetErrorCallback(ErrorCallback);
	}

	virtual ~GlfwApp() {
		if (nullptr != window) {
			glfwDestroyWindow(window);
		}
		glfwTerminate();
	}

	virtual int run() {
		preCreate();

		window = createRenderingTarget(windowSize, windowPosition);

		if (!window) {
			std::cout << "Unable to create OpenGL window" << std::endl;
			return -1;
		}

		postCreate();

		initGl();

		while (!glfwWindowShouldClose(window)) {
			++frame;
			glfwPollEvents();
			update();
			draw();
			finishFrame();
		}

		shutdownGl();

		return 0;
	}


protected:
	virtual GLFWwindow * createRenderingTarget(uvec2 & size, ivec2 & pos) = 0;

	virtual void draw() = 0;

	void preCreate() {
		glfwWindowHint(GLFW_DEPTH_BITS, 16);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
	}


	void postCreate() {
		glfwSetWindowUserPointer(window, this);
		glfwSetKeyCallback(window, KeyCallback);
		glfwSetMouseButtonCallback(window, MouseButtonCallback);
		glfwMakeContextCurrent(window);

		// Initialize the OpenGL bindings
		// For some reason we have to set this experminetal flag to properly
		// init GLEW if we use a core context.
		glewExperimental = GL_TRUE;
		if (0 != glewInit()) {
			FAIL("Failed to initialize GLEW");
		}
		glGetError();

		if (GLEW_KHR_debug) {
			GLint v;
			glGetIntegerv(GL_CONTEXT_FLAGS, &v);
			if (v & GL_CONTEXT_FLAG_DEBUG_BIT) {
				//glDebugMessageCallback(glDebugCallbackHandler, this);
			}
		}
	}

	virtual void initGl() {
	}

	virtual void shutdownGl() {
	}

	virtual void finishFrame() {
		glfwSwapBuffers(window);
	}

	virtual void destroyWindow() {
		glfwSetKeyCallback(window, nullptr);
		glfwSetMouseButtonCallback(window, nullptr);
		glfwDestroyWindow(window);
	}

	virtual void onKey(int key, int scancode, int action, int mods) {
		if (GLFW_PRESS != action) {
			return;
		}

		switch (key) {
		case GLFW_KEY_ESCAPE:
			glfwSetWindowShouldClose(window, 1);
			return;
		}
	}

	virtual void update() {}

	virtual void onMouseButton(int button, int action, int mods) {}

protected:
	virtual void viewport(const ivec2 & pos, const uvec2 & size) {
		glViewport(pos.x, pos.y, size.x, size.y);
	}

private:

	static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
		GlfwApp * instance = (GlfwApp *)glfwGetWindowUserPointer(window);
		instance->onKey(key, scancode, action, mods);
	}

	static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
		GlfwApp * instance = (GlfwApp *)glfwGetWindowUserPointer(window);
		instance->onMouseButton(button, action, mods);
	}

	static void ErrorCallback(int error, const char* description) {
		FAIL(description);
	}
};

//////////////////////////////////////////////////////////////////////
//
// The Oculus VR C API provides access to information about the HMD
//

class RiftManagerApp {
protected:
	ovrSession _session;
	ovrHmdDesc _hmdDesc;
	ovrGraphicsLuid _luid;

public:
	RiftManagerApp() {
		if (!OVR_SUCCESS(ovr_Create(&_session, &_luid))) {
			FAIL("Unable to create HMD session");
		}

		_hmdDesc = ovr_GetHmdDesc(_session);
	}

	~RiftManagerApp() {
		ovr_Destroy(_session);
		_session = nullptr;
	}
};

class RiftApp : public GlfwApp, public RiftManagerApp {
public:
	ovrLayerEyeFov _sceneLayer;
	ovrViewScaleDesc _viewScaleDesc;

	int aMode = 0;
	int bMode = 0;
	int xMode = 0;

	bool aPress = false;
	bool bPress = false;
	bool xPress = false;

	// Holds Position and Orientation
	ovrVector3f eyePositions[2];
	ovrQuatf eyeOrientations[2];

	ovrVector3f eyeOffsets[2];
	ovrVector3f originalEyeOffsets[2];

	bool rightGoingRight = false;
	bool rightGoingLeft = false;

	bool leftGoingRight = false;
	bool leftGoingLeft = false;

	ClientGame * client;

	bool handUse[2];
	bool fruitHeld[2];
	Drawable* heldFruit[2];

	vector<Drawable*> thrownFruit;

	glm::vec3 prevHandPos[2];
	glm::vec3 currHandPos[2];;

private:
	GLuint _fbo{ 0 };
	GLuint _depthBuffer{ 0 };
	ovrTextureSwapChain _eyeTexture;

	GLuint _mirrorFbo{ 0 };
	ovrMirrorTexture _mirrorTexture;

	ovrEyeRenderDesc _eyeRenderDescs[2];

	mat4 _eyeProjections[2];

	uvec2 _renderTargetSize;
	uvec2 _mirrorSize;


public:

	RiftApp() {
		using namespace ovr;
		_viewScaleDesc.HmdSpaceToWorldScaleInMeters = 1.0f;

		memset(&_sceneLayer, 0, sizeof(ovrLayerEyeFov));
		_sceneLayer.Header.Type = ovrLayerType_EyeFov;
		_sceneLayer.Header.Flags = ovrLayerFlag_TextureOriginAtBottomLeft;

		ovr::for_each_eye([&](ovrEyeType eye) {
			ovrEyeRenderDesc& erd = _eyeRenderDescs[eye] = ovr_GetRenderDesc(_session, eye, _hmdDesc.DefaultEyeFov[eye]);
			ovrMatrix4f ovrPerspectiveProjection =
				ovrMatrix4f_Projection(erd.Fov, 0.01f, 1000.0f, ovrProjection_ClipRangeOpenGL);
			_eyeProjections[eye] = ovr::toGlm(ovrPerspectiveProjection);
			_viewScaleDesc.HmdToEyeOffset[eye] = erd.HmdToEyeOffset;

			// IOD EYE OFFSETS
			eyeOffsets[eye] = erd.HmdToEyeOffset;
			originalEyeOffsets[eye] = erd.HmdToEyeOffset;

			ovrFovPort & fov = _sceneLayer.Fov[eye] = _eyeRenderDescs[eye].Fov;
			auto eyeSize = ovr_GetFovTextureSize(_session, eye, fov, 1.0f);
			_sceneLayer.Viewport[eye].Size = eyeSize;
			_sceneLayer.Viewport[eye].Pos = { (int)_renderTargetSize.x, 0 };

			_renderTargetSize.y = std::max(_renderTargetSize.y, (uint32_t)eyeSize.h);
			_renderTargetSize.x += eyeSize.w;
		});
		// Make the on screen window 1/4 the resolution of the render target
		_mirrorSize = _renderTargetSize;
		_mirrorSize /= 4;
	}

protected:
	GLFWwindow * createRenderingTarget(uvec2 & outSize, ivec2 & outPosition) override {
		return glfw::createWindow(_mirrorSize);
	}

	void initGl() override {
		GlfwApp::initGl();

		// Disable the v-sync for buffer swap
		glfwSwapInterval(0);

		ovrTextureSwapChainDesc desc = {};
		desc.Type = ovrTexture_2D;
		desc.ArraySize = 1;
		desc.Width = _renderTargetSize.x;
		desc.Height = _renderTargetSize.y;
		desc.MipLevels = 1;
		desc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
		desc.SampleCount = 1;
		desc.StaticImage = ovrFalse;
		ovrResult result = ovr_CreateTextureSwapChainGL(_session, &desc, &_eyeTexture);
		_sceneLayer.ColorTexture[0] = _eyeTexture;
		if (!OVR_SUCCESS(result)) {
			FAIL("Failed to create swap textures");
		}

		int length = 0;
		result = ovr_GetTextureSwapChainLength(_session, _eyeTexture, &length);
		if (!OVR_SUCCESS(result) || !length) {
			FAIL("Unable to count swap chain textures");
		}
		for (int i = 0; i < length; ++i) {
			GLuint chainTexId;
			ovr_GetTextureSwapChainBufferGL(_session, _eyeTexture, i, &chainTexId);
			glBindTexture(GL_TEXTURE_2D, chainTexId);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			// Generate mipmaps, by the way.
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		glBindTexture(GL_TEXTURE_2D, 0);

		// Set up the framebuffer object
		glGenFramebuffers(1, &_fbo);
		glGenRenderbuffers(1, &_depthBuffer);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo);
		glBindRenderbuffer(GL_RENDERBUFFER, _depthBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, _renderTargetSize.x, _renderTargetSize.y);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
		glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthBuffer);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

		ovrMirrorTextureDesc mirrorDesc;
		memset(&mirrorDesc, 0, sizeof(mirrorDesc));
		mirrorDesc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
		mirrorDesc.Width = _mirrorSize.x;
		mirrorDesc.Height = _mirrorSize.y;
		if (!OVR_SUCCESS(ovr_CreateMirrorTextureGL(_session, &mirrorDesc, &_mirrorTexture))) {
			FAIL("Could not create mirror texture");
		}
		glGenFramebuffers(1, &_mirrorFbo);

		for (int i = 0; i < 2; i++) {
			handUse[i] = false;
			fruitHeld[i] = false;
			heldFruit[i] = NULL;
		}

	}

	void onKey(int key, int scancode, int action, int mods) override {
		if (GLFW_PRESS == action) switch (key) {
		case GLFW_KEY_R:
			ovr_RecenterTrackingOrigin(_session);
			return;
		}

		GlfwApp::onKey(key, scancode, action, mods);
	}

	void draw() final override {
		ovrPosef eyePoses[2];
		ovr_GetEyePoses(_session, frame, true, _viewScaleDesc.HmdToEyeOffset, eyePoses, &_sceneLayer.SensorSampleTime);

		int eyeInd = 0;

		int curIndex;
		ovr_GetTextureSwapChainCurrentIndex(_session, _eyeTexture, &curIndex);
		GLuint curTexId;
		ovr_GetTextureSwapChainBufferGL(_session, _eyeTexture, curIndex, &curTexId);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo);
		glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, curTexId, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		ovr::for_each_eye([&](ovrEyeType eye) {
			const auto& vp = _sceneLayer.Viewport[eye];
			glViewport(vp.Pos.x, vp.Pos.y, vp.Size.w, vp.Size.h);
			_sceneLayer.RenderPose[eye] = eyePoses[eye];

			switch (bMode) {

			case 0:
				// POSTION + ORIENTATION
				eyePositions[eye] = eyePoses[eye].Position;
				eyeOrientations[eye] = eyePoses[eye].Orientation;
				break;

			case 1:
				// BOTH FIXED
				eyePoses[eye].Position = eyePositions[eye];
				eyePoses[eye].Orientation = eyeOrientations[eye];
				break;

			case 2:
				// POSITION FIXED
				eyePoses[eye].Position = eyePositions[eye];
				break;

			case 3:
				// ORIENTATION FIXED
				eyePoses[eye].Orientation = eyeOrientations[eye];
				break;
			}

			switch (aMode) {

			case 0:
				// STEREO
				renderScene(_eyeProjections[eye], ovr::toGlm(eyePoses[eye]));
				break;

			case 1:
				// MONO
				renderScene(_eyeProjections[eye], ovr::toGlm(eyePoses[ovrEye_Left]));
				break;

			case 2:
				// LEFT ONLY
				if (eye == ovrEye_Left) renderScene(_eyeProjections[eye], ovr::toGlm(eyePoses[eye]));
				break;

			case 3:
				// RIGHT ONLY
				if (eye == ovrEye_Right) renderScene(_eyeProjections[eye], ovr::toGlm(eyePoses[eye]));
				break;
			}

			// IOD
			if ((rightGoingRight == true) && (rightGoingLeft == false)) {
				if (eye == ovrEye_Left) {
					eyeOffsets[eye].x -= 0.01f;
					if (eyeOffsets[eye].x > 0.0f) eyeOffsets[eye].x = 0.0f;
					if (eyeOffsets[eye].x < -1.0f) eyeOffsets[eye].x = -1.0f;

				}
				else {
					eyeOffsets[eye].x += 0.01f;
					if (eyeOffsets[eye].x < 0.0f) eyeOffsets[eye].x = 0.0f;
					if (eyeOffsets[eye].x > 1.0f) eyeOffsets[eye].x = 1.0f;
				}
			}
			else if ((rightGoingRight == false) && (rightGoingLeft == true)) {
				if (eye == ovrEye_Left) {
					eyeOffsets[eye].x += 0.01f;
					if (eyeOffsets[eye].x > 0.0f) eyeOffsets[eye].x = 0.0f;
					if (eyeOffsets[eye].x < -1.0f) eyeOffsets[eye].x = -1.0f;
				}
				else {
					eyeOffsets[eye].x -= 0.01f;
					if (eyeOffsets[eye].x < 0.0f) eyeOffsets[eye].x = 0.0f;
					if (eyeOffsets[eye].x > 1.0f) eyeOffsets[eye].x = 1.0f;
				}
			}
			// UPDATE IOD
			_viewScaleDesc.HmdToEyeOffset[eye] = eyeOffsets[eye];
			eyeInd++;
		});
		glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		ovr_CommitTextureSwapChain(_session, _eyeTexture);
		ovrLayerHeader* headerList = &_sceneLayer.Header;
		ovr_SubmitFrame(_session, frame, &_viewScaleDesc, &headerList, 1);

		GLuint mirrorTextureId;
		ovr_GetMirrorTextureBufferGL(_session, _mirrorTexture, &mirrorTextureId);
		glBindFramebuffer(GL_READ_FRAMEBUFFER, _mirrorFbo);
		glFramebufferTexture2D(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mirrorTextureId, 0);
		glBlitFramebuffer(0, 0, _mirrorSize.x, _mirrorSize.y, 0, _mirrorSize.y, _mirrorSize.x, 0, GL_COLOR_BUFFER_BIT, GL_NEAREST);
		glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

		client->sendActionPackets();

	}

	void update() final override
	{
		Window::objUpdates.clear();
		//do game stuff
		client->update();

		ovrInputState inputState;

		double displayMidpointSeconds = ovr_GetPredictedDisplayTime(_session, frame);
		ovrTrackingState trackState = ovr_GetTrackingState(_session, displayMidpointSeconds, ovrTrue);

		ovrPosef leftHandPose = trackState.HandPoses[ovrHand_Left].ThePose;
		ovrPosef rightHandPose = trackState.HandPoses[ovrHand_Right].ThePose;

		ovrVector3f righty = rightHandPose.Position;
		glm::vec3 rightyVec = ovr::toGlm(righty);

		ovrVector3f lefty = leftHandPose.Position;
		glm::vec3 leftyVec = ovr::toGlm(lefty);

		if (OVR_SUCCESS(ovr_GetInputState(_session, ovrControllerType_Touch, &inputState)))
		{

			// Right Hand Trigger
			if (inputState.HandTrigger[ovrHand_Right] > 0.5f) {
				prevHandPos[1] = rightyVec;

				if (!fruitHeld[1]) {
					heldFruit[1] = Window::detectCollision_fruit(Window::gameObjects[4]);
					if (heldFruit[1] != NULL) {
						fruitHeld[1] = true;
					}
				}
				else {
					heldFruit[1]->position = rightyVec;
					if (heldFruit[1]->model == Window::apple) {
						heldFruit[1]->position.x = heldFruit[1]->position.x - 0.25f;
						heldFruit[1]->position.y = heldFruit[1]->position.y - 0.18f;
						heldFruit[1]->position.z = heldFruit[1]->position.z + 0.175f;
					}
					else if (heldFruit[1]->model == Window::orange) {
						heldFruit[1]->position.x = heldFruit[1]->position.x - 0.1f;
						heldFruit[1]->position.y = heldFruit[1]->position.y - 0.1f;
						heldFruit[1]->position.z = heldFruit[1]->position.z + 0.15f;
					}
					else if (heldFruit[1]->model == Window::pear) {
						heldFruit[1]->position.x = heldFruit[1]->position.x - 0.1f;
						heldFruit[1]->position.y = heldFruit[1]->position.y - 0.1f;
						heldFruit[1]->position.z = heldFruit[1]->position.z + 0.15f;
					}
					Window::push_to_updates(heldFruit[1]);
				}
			}
			else {
				currHandPos[1] = rightyVec;

				if (fruitHeld[1]) {
					glm::vec3 veloc = 0.7f * (currHandPos[1] - prevHandPos[1]);
					heldFruit[1]->velocity = veloc;

					thrownFruit.push_back(heldFruit[1]);

					heldFruit[1] = NULL;
					fruitHeld[1] = false;
				}
			}
			
			// Left Hand Trigger
			if (inputState.HandTrigger[ovrHand_Left] > 0.5f) {
				prevHandPos[0] = leftyVec;
	
				if (!fruitHeld[0]) {
					heldFruit[0] = Window::detectCollision_fruit(Window::gameObjects[5]);
					if (heldFruit[0] != NULL) {
						fruitHeld[0] = true;
					}
				}
				else {
					heldFruit[0]->position = leftyVec;
					if (heldFruit[0]->model == Window::apple) {
						heldFruit[0]->position.x = heldFruit[0]->position.x - 0.25f;
						heldFruit[0]->position.y = heldFruit[0]->position.y - 0.18f;
						heldFruit[0]->position.z = heldFruit[0]->position.z + 0.175f;
					}
					else if (heldFruit[0]->model == Window::orange) {
						heldFruit[0]->position.x = heldFruit[0]->position.x - 0.1f;
						heldFruit[0]->position.y = heldFruit[0]->position.y - 0.1f;
						heldFruit[0]->position.z = heldFruit[0]->position.z + 0.15f;
					}
					else if (heldFruit[0]->model == Window::pear) {
						heldFruit[0]->position.x = heldFruit[0]->position.x - 0.1f;
						heldFruit[0]->position.y = heldFruit[0]->position.y - 0.1f;
						heldFruit[0]->position.z = heldFruit[0]->position.z + 0.15f;
					}
					Window::push_to_updates(heldFruit[0]);
				}
			}
			else {
				currHandPos[0] = leftyVec;
				if (fruitHeld[0]) {
					glm::vec3 veloc = 0.5f * (currHandPos[0] - prevHandPos[0]);
					heldFruit[0]->velocity = veloc;

					thrownFruit.push_back(heldFruit[0]);

					heldFruit[0] = NULL;
					fruitHeld[0] = false;
				}
			}

		}
	}

	void throwFruit() {
		for (int i = 0; i < thrownFruit.size(); i++) {
			thrownFruit[i]->move();
			Window::push_to_updates(thrownFruit[i]);
		}
	}

	void resetIOD() {
		eyeOffsets[ovrEye_Left] = originalEyeOffsets[ovrEye_Left];
		eyeOffsets[ovrEye_Right] = originalEyeOffsets[ovrEye_Right];
	}

	virtual void renderScene(const glm::mat4 & projection, const glm::mat4 & headPose) = 0;
};

namespace Attribute {
	enum {
		Position = 0,
		TexCoord0 = 1,
		Normal = 2,
		Color = 3,
		TexCoord1 = 4,
		InstanceTransform = 5,
	};
}

// An example application that renders a simple cube
class ExampleApp : public RiftApp {
	//std::shared_ptr<ColorCubeScene> cubeScene;

public:
	ExampleApp() { }


protected:
	void initGl() override {
		client = new ClientGame();
		RiftApp::initGl();
		Window::initialize(_session);

	}

	void shutdownGl() override {
		//cubeScene.reset();
	}

	void resetState() {
		Window::reset(_session);
	}


	void keyCallback() {
		ovrInputState inputState;

		ovrPosef eyePoses[2];
		ovr_GetEyePoses(_session, frame, true, _viewScaleDesc.HmdToEyeOffset, eyePoses, &_sceneLayer.SensorSampleTime);

		if (OVR_SUCCESS(ovr_GetInputState(_session, ovrControllerType_Touch, &inputState))) {
		}
	}


	void renderScene(const glm::mat4 & projection, const glm::mat4 & headPose) override {

		// Touch controller schtuff

		double displayMidpointSeconds = ovr_GetPredictedDisplayTime(_session, frame);
		ovrTrackingState trackState = ovr_GetTrackingState(_session, displayMidpointSeconds, ovrTrue);

		ovrPosef leftHandPose = trackState.HandPoses[ovrHand_Left].ThePose;
		ovrPosef rightHandPose = trackState.HandPoses[ovrHand_Right].ThePose;
		ovrPosef headPose2 = trackState.HeadPose.ThePose;
		
		glm::quat myQuat(rightHandPose.Orientation.w, rightHandPose.Orientation.x, rightHandPose.Orientation.y, rightHandPose.Orientation.z);


		if (Window::gameObjects[4] != nullptr) { // right hand
			Window::gameObjects[4]->position = ovr::toGlm(rightHandPose.Position);
			Window::gameObjects[4]->rotation = glm::rotate(glm::toMat4(myQuat), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
			Window::objUpdates.push_back(Window::gameObjects[4]);
		}

		if (Window::gameObjects[5] != nullptr) { // left hand
			myQuat = glm::quat(leftHandPose.Orientation.w, leftHandPose.Orientation.x, leftHandPose.Orientation.y, leftHandPose.Orientation.z);
			Window::gameObjects[5]->position = ovr::toGlm(leftHandPose.Position);
			Window::gameObjects[5]->rotation = glm::rotate(glm::toMat4(myQuat), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
			Window::objUpdates.push_back(Window::gameObjects[5]);
		}

		if (Window::gameObjects[3] != nullptr) { // head
			myQuat = glm::quat(headPose2.Orientation.w, headPose2.Orientation.x, headPose2.Orientation.y, headPose2.Orientation.z);
			Window::gameObjects[3]->position = ovr::toGlm(headPose2.Position);
			Window::gameObjects[3]->rotation = glm::rotate(glm::toMat4(myQuat), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
			Window::objUpdates.push_back(Window::gameObjects[3]);
		}

		Window::displayCallback(projection, headPose, _session, frame, xMode);

		throwFruit();
	}
	void onKey(int key, int scancode, int action, int mods) override {
		if (GLFW_PRESS == action) switch (key) {
		case GLFW_KEY_R:

		case GLFW_KEY_T: // debug key that prints current head position and orientation

			ovrPosef eyePoses[2];
			ovr_GetEyePoses(_session, frame, true, _viewScaleDesc.HmdToEyeOffset, eyePoses, &_sceneLayer.SensorSampleTime);

			char buff[100];
			sprintf_s(buff, "(%f, %f, %f)\n", eyePoses[0].Position.x, eyePoses[0].Position.y, eyePoses[0].Position.z);
			OutputDebugStringA(buff);

			sprintf_s(buff, "(%f, %f, %f)\n", eyePoses[1].Position.x, eyePoses[1].Position.y, eyePoses[1].Position.z);
			OutputDebugStringA(buff);

			sprintf_s(buff, "(%f, %f, %f, %f)\n", eyePoses[0].Orientation.x, eyePoses[0].Orientation.y, eyePoses[0].Orientation.z, eyePoses[0].Orientation.w);
			OutputDebugStringA(buff);

			sprintf_s(buff, "(%f, %f, %f, %f)\n", eyePoses[1].Orientation.x, eyePoses[1].Orientation.y, eyePoses[1].Orientation.z, eyePoses[1].Orientation.w);

			OutputDebugStringA(buff);
			return;
		}

		GlfwApp::onKey(key, scancode, action, mods);
	}
};

// Execute our example class
int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {

	int result = -1;
	try {
		if (!OVR_SUCCESS(ovr_Initialize(nullptr))) {
			FAIL("Failed to initialize the Oculus SDK");
		}
		result = ExampleApp().run();
	}
	catch (std::exception & error) {
		OutputDebugStringA(error.what());
		std::cerr << error.what() << std::endl;
	}
	ovr_Shutdown();
	return result;
}