#pragma once
#include <string.h>
#include <glm/glm.hpp>
#include <glm/mat4x4.hpp>

#define MAX_PACKET_SIZE 1000000

enum PacketTypes {

    INIT_CONNECTION = 0,

    ACTION_EVENT = 1,

	RESET = 2

};

struct Packet {
	unsigned int packet_type;

	unsigned int objID;

	glm::vec3 pos;
	glm::vec3 velocity;
	glm::mat4 rotation;
	float scalar;
	float enabled;

	void serialize(char * data) {
		memcpy(data, this, sizeof(Packet));
	}

	void deserialize(char * data) {
		memcpy(this, data, sizeof(Packet));
	}

};
