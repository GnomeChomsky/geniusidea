#include "Object.h"

Object::Object(unsigned int objID, glm::vec3 pos, float scalar) {
	objectID = objID;
	position = pos;
	scale = scalar;

	velocity = glm::vec3(0.0f);
	rotation = glm::mat4(1.0f);
	enabled = true;
}

Packet Object::convertToPacket() {
	Packet packet;
	packet.packet_type = ACTION_EVENT;
	packet.objID = objectID;
	packet.pos = position;
	packet.scalar = scale;
	packet.rotation = rotation;
	packet.velocity = velocity;
	if(enabled) packet.enabled = 1.0f;
	else packet.enabled = 0.0f;

	return packet;
}

void Object::update(Packet packet) {
	position = packet.pos;
	scale = packet.scalar;

	velocity = packet.velocity;
	rotation = packet.rotation;
	if (packet.enabled == 0.0f) enabled = false;
}