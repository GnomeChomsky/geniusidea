#pragma once

#include "../Network/NetworkData.h"


class Object
{
public:
	Object(unsigned int, glm::vec3, float);

	glm::vec3 position; // translation (offset from 0)
	glm::mat4 rotation; // local rotation
	float scale; // scalar value for dimensional proportion

	glm::vec3 velocity;

	unsigned int objectID;
	bool enabled;

	void update(Packet);
	Packet convertToPacket();
};