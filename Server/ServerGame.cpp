#include "../Network/stdafx.h"
#include "ServerGame.h"

unsigned int ServerGame::client_id; 
std::vector<Object*> ServerGame::gameObjects;

ServerGame::ServerGame(void)
{
    // id's to assign clients for our table
    client_id = 0;
	Object* temp;
	for (int i = 0; i < 20; i++) {
		glm::vec3 position(0.0f, 0.5f * i, 0.0f);
		float scale;
		switch (i) {
		case 0: // DOJO
			position = glm::vec3(-3.3f, -0.75f, -5.5f);
			scale = 1.0f;
			break;
		case 1: // DOJO MIRROR
			position = glm::vec3(3.6f, -0.75f, 5.5f);
			scale = 1.0f;
			break;
		case 2: // CRATE
			position = glm::vec3(0.1f, -0.40f, 0.0f);
			scale = 0.0015f;
			break;
		case 3: // Oculus Head
			//position = glm::vec3(0.0f, 0.0f, 0.0f);
			scale = 0.1f;
			break;
		case 4: // Oculus Hand1
			position = glm::vec3(-0.7f, 1.0f, 0.0f);
			scale = 10.0f;
			break;
		case 5: // Oculus Hand2
			position = glm::vec3(0.75f, 0.5f, 0.0f);
			scale = 10.0f;
			break;
		case 6: // Leap Head
			position = glm::vec3(0.0f, 0.5f, -6.75f);
			scale = 0.1f;
			break;
		case 7: // Leap Hand
			position = glm::vec3(0.0f, 0.0f, -5.75f);
			scale = 0.01f;
			break;
		case 8: // apple1
			position = glm::vec3(0.1125f, -0.41f, -0.2f);
			scale = 0.0015f;
			break;
		case 9: // apple2
			position = glm::vec3(-0.01f, -0.41f, -0.015f);
			scale = 0.0015f;
			break;
		case 10: // apple3
			position = glm::vec3(-0.01f, -0.5f, -0.0155f);
			scale = 0.0015f;
			break;
		case 11: // apple4
				 //position = glm::vec3(0.0f, 0.0f, 0.0f);
			scale = 0.0015f;
			break;
		case 12: // orange1
			position = glm::vec3(0.1f, -0.375f, -0.3525f);
			scale = 0.0005f;
			break;
		case 13: // orange2
			position = glm::vec3(0.25f, -0.375f, -0.04f);
			scale = 0.0005f;
			break;
		case 14: // orange3
			position = glm::vec3(0.165f, -0.3f, -0.34f);
			scale = 0.0005f;
			break;
		case 15: // orange4
				 //position = glm::vec3(0.0f, 0.0f, 0.0f);
			scale = 0.0005f;
			break;
		case 16: // Pear1
			position = glm::vec3(0.25f, -0.375f, -0.35f);
			scale = 0.05f;
			break;
		case 17: // pear2
			position = glm::vec3(0.0975f, -0.375f, -0.245f);
			scale = 0.05f;
			break;
		case 18: // pear3
			position = glm::vec3(0.25f, -0.3f, -0.215f);
			scale = 0.05f;
			break;
		case 19: // pear4
			//position = glm::vec3(0.0f, 0.5f, -4.75f);
			scale = 0.05f;
			break;
		default:
			//position = glm::vec3(0.0f, 0.0f, 0.0f);
			scale = 1.0f;
			break;
		}
		temp = new Object(i, position, scale);
		gameObjects.push_back(temp);
	}

	// lol flip the dojo mirror here
	gameObjects[1]->rotation = glm::rotate(glm::mat4(), glm::radians(180.0f), glm::vec3(0.0f,1.0f,0.0f));

    // set up the server network to listen 
    network = new ServerNetwork(); 
}

void ServerGame::reset() {
	// id's to assign clients for our table
	client_id = 0;
	Object* temp;
	for (int i = 0; i < 20; i++) {
		glm::vec3 position(0.0f, 0.5f * i, 0.0f);
		float scale;
		switch (i) {
		case 0: // DOJO
			position = glm::vec3(-3.3f, -0.75f, -5.5f);
			scale = 1.0f;
			break;
		case 1: // DOJO MIRROR
			position = glm::vec3(3.6f, -0.75f, 5.5f);
			scale = 1.0f;
			break;
		case 2: // CRATE
			position = glm::vec3(0.1f, -0.40f, 0.0f);
			scale = 0.0015f;
			break;
		case 3: // Oculus Head
				//position = glm::vec3(0.0f, 0.0f, 0.0f);
			scale = 0.1f;
			break;
		case 4: // Oculus Hand1
			position = glm::vec3(-0.7f, 1.0f, 0.0f);
			scale = 10.0f;
			break;
		case 5: // Oculus Hand2
			position = glm::vec3(0.75f, 0.5f, 0.0f);
			scale = 10.0f;
			break;
		case 6: // Leap Head
			position = glm::vec3(0.0f, 0.5f, -6.75f);
			scale = 0.1f;
			break;
		case 7: // Leap Hand
			position = glm::vec3(0.0f, 0.0f, -5.75f);
			scale = 0.01f;
			break;
		case 8: // apple1
			position = glm::vec3(0.1125f, -0.41f, -0.2f);
			scale = 0.0015f;
			break;
		case 9: // apple2
			position = glm::vec3(-0.01f, -0.41f, -0.015f);
			scale = 0.0015f;
			break;
		case 10: // apple3
			position = glm::vec3(-0.01f, -0.5f, -0.0155f);
			scale = 0.0015f;
			break;
		case 11: // apple4
				 //position = glm::vec3(0.0f, 0.0f, 0.0f);
			scale = 0.0015f;
			break;
		case 12: // orange1
			position = glm::vec3(0.1f, -0.375f, -0.3525f);
			scale = 0.0005f;
			break;
		case 13: // orange2
			position = glm::vec3(0.25f, -0.375f, -0.04f);
			scale = 0.0005f;
			break;
		case 14: // orange3
			position = glm::vec3(0.165f, -0.3f, -0.34f);
			scale = 0.0005f;
			break;
		case 15: // orange4
				 //position = glm::vec3(0.0f, 0.0f, 0.0f);
			scale = 0.0005f;
			break;
		case 16: // Pear1
			position = glm::vec3(0.25f, -0.375f, -0.35f);
			scale = 0.05f;
			break;
		case 17: // pear2
			position = glm::vec3(0.0975f, -0.375f, -0.245f);
			scale = 0.05f;
			break;
		case 18: // pear3
			position = glm::vec3(0.25f, -0.3f, -0.215f);
			scale = 0.05f;
			break;
		case 19: // pear4
			position = glm::vec3(0.0f, 0.5f, -4.75f);
			scale = 0.05f;
			break;
		default:
			//position = glm::vec3(0.0f, 0.0f, 0.0f);
			scale = 1.0f;
			break;
		}
		gameObjects[i]->position = position;
		gameObjects[i]->scale = scale;

	}

	// lol flip the dojo mirror here
	gameObjects[1]->rotation = glm::rotate(glm::mat4(), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
}

ServerGame::~ServerGame(void)
{
	delete network;
}

void ServerGame::update()
{
    // get new clients
   if(network->acceptNewClient(client_id))
   {
        printf("client %d has been connected to the server\n",client_id);

        client_id++;
   }

   receiveFromClients();
}

void ServerGame::processPacket(Packet packet) {
	gameObjects[packet.objID]->update(packet);
}

void ServerGame::receiveFromClients()
{

    Packet packet;

    // go through all clients
    std::map<unsigned int, SOCKET>::iterator iter;

    for(iter = network->sessions.begin(); iter != network->sessions.end(); iter++)
    {
        int data_length = network->receiveData(iter->first, network_data);

        if (data_length <= 0) 
        {
            //no data recieved
            continue;
        }

        int i = 0;
		int pack_num = 0;
        while (i < (unsigned int)data_length) 
        {
            packet.deserialize(&(network_data[i]));
            i += sizeof(Packet);
			pack_num++;
            switch (packet.packet_type) {

                case INIT_CONNECTION:

                    printf("server received init packet from client\n");

                    break;

                case ACTION_EVENT:

                    //printf("server received action event packet from client\n");
					// update internal representation of game
					processPacket(packet);
                    break;

				case RESET:
					printf("RESETTING STATE!!\n");
					reset();
					break;

                default:

                    printf("error in packet types\n");

                    break;
            }
        }
		//printf("NUMBER OF PACKETS RECEIVED: %d\n", pack_num);

		sendInitGame();

    }
}

void ServerGame::sendInitGame() {
	// send action packet
	const unsigned int packet_size = 20 * sizeof(Packet);
	char packet_data[packet_size];

	Packet packets[20];

	for (int i = 0; i < 20; i++) {
		packets[i] = gameObjects[i]->convertToPacket();
		packets[i].serialize(&(packet_data[i * sizeof(Packet)]));
	}
	//printf("Deserializing...\n");
	//for (int i = 0; i < 20; i++) {
	//	packets[i].deserialize(&(packet_data[i * sizeof(Packet)]));
	//	printf("Packet type: %d\n", packets[i].packet_type);
	//}

	network->sendToAll(packet_data, packet_size);
}

void ServerGame::sendActionPackets()
{
	// send action packet
	const unsigned int packet_size = sizeof(Packet);
	char packet_data[packet_size];

	Packet packets;
	packets.packet_type = ACTION_EVENT;

	packets.serialize(packet_data);

	network->sendToAll(packet_data, packet_size);
}