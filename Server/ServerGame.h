#pragma once
#include "ServerNetwork.h"
#include "../Network/NetworkData.h"
#include "Object.h"
#include <vector>
#include <glm/gtc/matrix_transform.hpp>

class ServerGame
{

public:

    ServerGame(void);
    ~ServerGame(void);

    void update();

	void reset();

	void processPacket(Packet);
	void receiveFromClients();
	void sendInitGame();
	void sendActionPackets();

private:

   // IDs for the clients connecting for table in ServerNetwork 
    static unsigned int client_id;

	static std::vector<Object*> gameObjects;

   // The ServerNetwork object 
    ServerNetwork* network;

	// data buffer
   char network_data[MAX_PACKET_SIZE];
};