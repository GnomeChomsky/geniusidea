// main.cpp : Defines the entry point for the console application.
//

// may need #include "stdafx.h" in visual studio
#include "../Network/stdafx.h"
#include "ServerGame.h"

void serverLoop(void *);

ServerGame * server;

int main()
{

	// initialize the server
	server = new ServerGame();

	serverLoop((void*)12);

}

void serverLoop(void * arg) 
{ 
    while(true) 
    {
        server->update();
    }
}
